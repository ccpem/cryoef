/****************************************** cryoEF.cc ************************************************/
/* Copyright (c) 2017 Christopher J. Russo, Katerina Naydenova, MRC Laboratory of Molecular Biology  */
/* See accompanying LICENSE file for terms of use.                                                   */
/* Version 1.1.0  -  revised 18 October 2017                                                          */
/*                                                                                                   */
/*                                                                                                   */
/* On most machines, compile with the command:                                                       */
/* make                                                                                              */
/*****************************************************************************************************/

#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <fftw3.h>
#include <cstring>
#include <ctime>
//custom headers:
#include "maths.h"
#include "arrays3d.h"
#include "iofiles.h"
#include "sorting.h"
#include "rescalc.h"
#include "symmetries.h"

/*****K-space signal modulation: Rosenthal & Henderson, 2003*****/
double SamplingFun (int n, double r, double B)
{
    double C=sqrt(n);
    return C*C*exp(-B*r*r/2); // returning the value^2 to keep linearity in N
}

/*****Fills in values into K-space density map*****/
void CalcMap (double a, double b, double c, int multiplicity, Array3D& dmap, int dim, double B)
{
    Array3D arr(dim,dim,dim,0); //array for calculation at gridpoints
    int i, j, k;
    int o=dim/2;
    double rad, rmax, val;
    rmax=o-2; //padding
    double x,y,z;
    std::vector<int> icoord, jcoord, kcoord;
    std::vector<double> radcoord;
    icoord.reserve(2*dim*dim);
    jcoord.reserve(2*dim*dim);
    kcoord.reserve(2*dim*dim);
    
    //go through whole grid and store signs of the plane equation a*x+b*y+c*z at each vertex:
    for (i=0; i<dim; ++i)
      { x=a*(i-o+0.5);
        for(j=0; j<dim; ++j)
	  {
	    y=b*(j-o+0.5);
            for(k=0; k<dim; ++k)
            {
	        z=c*(k-o+0.5);
	        val=x+y+z;
	        if(val>0) arr(i,j,k)=1;
                else
                {
                    if(val<0) arr(i,j,k)=-1;
                    /*else arr(i,j,k)=0;*/ //unneccessary..already initialized with zeroes
                }
            };
	  };
      };
    
    //go through whole grid again and find traversed voxels by comparing the signs at each 8 neighbouring vertices (count how many are traversed to normalize by this number later)
    int count=0;
    double x2, y2, z2;
    for (i=1; i<dim; ++i)
      {
	x2=(i-o)*(i-o);
        for(j=1; j<dim; ++j)
	  {
	    y2=(j-o)*(j-o);
            for(k=1; k<dim; ++k)
            {
	        z2=(k-o)*(k-o);
	        rad=sqrt(x2+y2+z2);
                if (rad<rmax) if(!SameSigns(arr(i,j,k),arr(i-1,j,k),arr(i,j-1,k),arr(i,j,k-1),arr(i-1,j-1,k),arr(i-1,j,k-1),arr(i,j-1,k-1),arr(i-1,j-1,k-1))) {count++; icoord.push_back(i); jcoord.push_back(j); kcoord.push_back(k); radcoord.push_back(rad);}
	    };
	  };
      };
    //TODO : IS THE COUNT CORRECTION NEEDED OR NOT? MAKES PROGRAM almost 2x SLOWER.
    //MIGHT BE BETTER TO REMEMBER ADDRESSES OF TRAVERSED VOXELS
    for (i=0; i<count; i++)
      dmap(icoord[i],jcoord[i],kcoord[i])+=SamplingFun(multiplicity,radcoord[i],B)/count;
    
    //go throgh the grid one more time to put in the values of the sampling function into the traversed voxels
    /*for (i=1; i<dim; ++i)
        for(j=1; j<dim; ++j)
            for(k=1; k<dim; ++k)
            {
                rad=sqrt((i-o)*(i-o)+(j-o)*(j-o)+(k-o)*(k-o));
                if (rad<rmax) if(!SameSigns(arr(i,j,k),arr(i-1,j,k),arr(i,j-1,k),arr(i,j,k-1),arr(i-1,j-1,k),arr(i-1,j,k-1),arr(i,j-1,k-1),arr(i-1,j-1,k-1))) dmap(i,j,k)+=SamplingFun(multiplicity,rad,B)/count;
		};*/
    
}

/*****Function to bin angular data and calculate corresponding plane coefficients*****/
int CompressAndCalc(std::vector<int>& theta, std::vector<int>& phi, std::vector<double>& a, std::vector<double>& b, std::vector<double>& c, std::vector<int>& bin_val, int n)
{
    int theta_now, phi_now, bin_val_now;
    double x, y, z;
    int j;
    int i=0;
    theta_now=theta[0];
    phi_now=phi[0];
    bin_val_now=1;
    
    //the list (phi, theta) comes in already sorted
    for (j=1; j<n; ++j)
    {
        if(theta[j]==theta_now && phi[j]==phi_now) ++bin_val_now;
        else
            //each time when a new entry is encountered - calculate and remember plane coefficients and the multiplicity
        {
            Euler2Dir(phi_now, theta_now, x, y, z);
            a.push_back(x);
            b.push_back(y);
            c.push_back(z);
            bin_val.push_back(bin_val_now);
            //std::cout<<"Entry No:"<<i+1<<" THETA="<<std::setw(3)<<theta_now<<" PHI="<<std::setw(3)<<phi_now<<" No of datapoints:"<<std::setw(3)<<bin_val[i]/*<<" A="<<a[i]<<" B="<<b[i]<<" C="<<c[i]*/<<std::endl;
            ++i;
            phi_now=phi[j];
            theta_now=theta[j];
            bin_val_now=1;
        }
    };
    
    //required to store the last unique entry
    Euler2Dir(phi_now,theta_now,x,y,z);
    a.push_back(x);
    b.push_back(y);
    c.push_back(z);
    bin_val.push_back(bin_val_now);
    //std::cout<<"Entry No:"<<i+1<<" THETA="<<std::setw(3)<<theta_now<<" PHI="<<std::setw(3)<<phi_now<<" No of datapoints:"<<std::setw(3)<<bin_val[i]/*<<" A="<<a[i]<<" B="<<b[i]<<" C="<<c[i]*/<<std::endl;
    int imax=i+1;
    return imax; //returns the total number of *unique* entries
}

/*****Function which is called from main if symmetry is indicated in input*****/
void HandleSymmerty (const char* sym, std::vector<double>& a, std::vector<double>& b, std::vector<double>& c, std::vector<int>& bin_val, int nviews, std::ofstream& logfile)
{
    int pgGroup, pgOrder;
    
    if(IsSymmetryGroup(sym, pgGroup, pgOrder)) //check what symmetry is input after the -g flag
    {
        SymmetryMatrices(pgGroup, pgOrder, a, b, c, bin_val, nviews); //generate and apply the corresponding transform matrices
        logfile << "Symmetry transformations applied" << std::endl;
    }
    else
    {
        logfile << "Symmetry group unknown" << std::endl;
        std::cerr << "Symmetry group unknown" << std::endl;
        exit(0);
    }
    
}

/*****Auto threfholding: finds the 1/e^2 point in the PSF*****/
double FindThreshold (Array3D& dmap, int dim)
{
    
    int i, j, k;
    double max=0;
    
    for (i=0; i<dim; ++i)
        for(j=0; j<dim; ++j)
            for(k=0; k<dim; ++k)
                if(dmap(i,j,k)>max) max=dmap(i,j,k);
    
    double thr_factor=1.0/pow(M_E,2); //can edit PSF threshold level here
    double thr=max*thr_factor;
    return thr;
}

/*****Function called from main - produces all output info about the resolution*****/
void ResolutionCalculation(int n, Array3D& Rmap, int dim, double& thr, double Apix, int da, double tiltmax, double res_FSC, bool res_flag, std::ofstream& logfile, std::string& nameoffile)
{
    int i, j, k, m;
    double resmap[180][180]={0}; //mapping resolution by (phi,theta)
    if (thr==0) thr = FindThreshold(Rmap, dim);
    double area;
    double res_worst, res_best, res_step, res;
    int nbins=10;
    std::vector<int> resdistrib (nbins);
    int phimax, thetamax, phimin, thetamin;
    double scaling;
    double average=0;
    
    Array3D Rsurf(dim, dim, dim, 0);

            Thresholding(Rmap, dim, thr); //discards all entries below the threshold
            Rsurf = Rmap;
            GetSurface(Rsurf, dim); //extracts the surface from the thresholded map
            
            for (m=0; m<nbins; ++m) resdistrib[m]=0;
            
            //find worst and best resolution:
            res_worst = FindWorstRes(Rsurf, dim, phimax, thetamax);
            res_best = FindBestRes(Rsurf, dim, phimin, thetamin);
        
            //generate the histogram of resolution distribution:
            res_step = (res_worst-res_best)/nbins;
            for (k=0; k<dim; ++k)
                for (j=0; j<dim; ++j)
                    for (i=0; i<dim; ++i)
                        if(Rsurf(i, j, k)!=0)
                        {
                            res = sqrt(pow(i-dim/2, 2.0)+pow(j-dim/2, 2.0)+pow(k-dim/2, 2.0));
                            average+=res;
                            m = int((res-res_best)/res_step);
                            if (m>=nbins) m=nbins-1;
                            resdistrib[m]++;
                        }
        
    logfile << "Thresholding at: " << std::fixed << std::setprecision(6) << thr << " done" << std::endl; //use thr value for viewing in Chimera
    
    area = CalcArea(Rsurf, dim);
    average = average/area; //get the mean (effectively area = no. of datapoints)
    //if the user input their FSC resolution, scale the mean to their input. TODO: Scale B instead of res_mean to match res_FSC
    if (res_flag)
        Apix=res_FSC/average;    

    //find the standard deviation of the distribution of resolutions - used for quality assessment
    std::ofstream rf;
    const char* HistFile = nameoffile.c_str();
    rf.open(HistFile);
    double stdev=0;
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i)
                if(Rsurf(i, j, k)!=0)
                {
                    res = sqrt(pow(i-dim/2, 2.0)+pow(j-dim/2, 2.0)+pow(k-dim/2, 2.0));
                    stdev+=pow(res-average, 2);
                    rf << res*Apix << std::endl;
                }
    rf.close();
    stdev=stdev/area;
    stdev=sqrt(stdev);
    
    logfile << std::endl << "------------------------------------ Output ------------------------------------" << std::endl;
    logfile << "Efficiency: " << std::fixed << std::setprecision(2) << 1-2*stdev/average << std::endl;
    logfile << "Mean PSF resolution: " << std::fixed << std::setprecision(2) << average*Apix << " Å" << std::endl;
    logfile << "Standard deviation: " << stdev*Apix << " Å" << std::endl;
    std::cerr << "Efficiency: " << 1-2*stdev/average << std::endl;
    
    logfile << "Worst PSF resolution: " << res_worst*Apix << " Å";
    logfile << " Direction: phi = " << phimax << " theta = " << thetamax << std::endl;
    logfile << "Best PSF resolution: " << res_best*Apix << " Å";
    logfile << " Direction: phi = " << phimin << " theta = " << thetamin << std::endl;
    
    if (res_worst-res_best<2) std::cerr << "Warning: May need larger box size. Currently best and worst resolution differ by " << res_worst-res_best << " pixels only" << std::endl;
    
    logfile << "Distribution of PSF resolution: " << std::endl;
    for (m=0; m<nbins; ++m)
        logfile << (res_best+m*res_step)*Apix << " Å - " << (res_best+(m+1)*res_step)*Apix << " Å: " << 100*double(resdistrib[m])/double(area) << "%" << std::endl;
    
    logfile << std::endl << "------------------------------ Other Output Files ------------------------------" << std::endl;
    logfile << "Data for PSF resolution histogram written to file " << nameoffile << std::endl;
    for (i=1; i<=10; i++) nameoffile.erase(nameoffile.end()-1);
    logfile << "Fourier space information density written to file " << nameoffile << "K.mrc" << std::endl;
    logfile << "Real space PSF written to file " << nameoffile << "R.mrc" << std::endl;
    ResolutionMap(Rsurf, dim, resmap, da, Apix); //sample the function resolution(phi,theta)
    SuggestTilt(n, resmap, phimax, thetamax, tiltmax, da, res_worst, Apix, logfile); //suggest tilt angles by looking at the best resolved directions accessible from the worst direction by tilting (takes into account 1/cos(tiltangle) fall-of in quality)
}


int main (int argc, char** argv)
{
    /*****input variables*****/
    int dim=128; 		//default box size (preferably even)
    int dimpad=dim; 		//padding size
    double D=200; 		// object size in A
    int da=1; 			//angular bin size (degrees)
    double B=160; 		//B factor
    double thr=0; 		//threshold;
    const char* inFile=""; 	//input file name
    const char* sym=""; 	//type of symmetry
    double tiltmax=45; 		//max allowed tilt angle (deg)
    double res_FSC; 		//FSC resolution in Angstrom
    double W=1; 		//weighting const for finding outlier points
    bool flag_outliers=false; 	//outlier removal feature on/off
    
    /*****computation variables*****/
    int i,j,k;
    int n; 			//no of entries in file
    int nviews; 		//total number of different projections
    bool sym_flag=0; 		//symmetry operations
    double Beff; 		//B-scaling so that 1 pix in k-space = 1/D
    double Apix; 		//A/pix in R-space PSF
    bool nosize_flag=1;
    bool res_flag=0; 		//user input resolution flag
    
    /*****command inputs*****/
    if (argc<2) //if no arguments: print help and exit
    {
        std::cerr << "cryoEF - efficiency analyzer for particle orientation distributions" << std::endl;
        std::cerr << "See Naydenova & Russo 2017 for more information" << std::endl;
	std::cerr << "" << std::endl; 
	std::cerr << "Required inputs" << std::endl; 
	std::cerr << "" << std::endl; 
	std::cerr << "      -f        Input filename containing list of orientaion angles" << std::endl; 
	std::cerr << "" << std::endl; 
	std::cerr << "Optional inputs [default value] Description" << std::endl; 
	std::cerr << "" << std::endl; 
	std::cerr << "      -b        [128]           Box size in pixels" << std::endl; 
	std::cerr << "      -a        [1]             Angular accuracy in degrees" << std::endl; 
	std::cerr << "      -B        [160]           B-factor in Angstroms^2" << std::endl; 
	std::cerr << "      -D        [200]           Particle diameter in Angstroms" << std::endl; 
	std::cerr << "      -g        [C1]            Particle symmetry group (e.g. C1)" << std::endl; 
	std::cerr << "      -m        [45]            Maximum tilt angle in degrees" << std::endl; 
	std::cerr << "      -r        [from B-factor] FSC resolution in Angstroms" << std::endl; 
	std::cerr << "" << std::endl; 
	std::cerr << "Example usage" << std::endl; 
	std::cerr << "" << std::endl; 
	std::cerr << "      > cryoEF -f filename.dat" << std::endl; 
	std::cerr << "" << std::endl; 
	std::cerr << "Outputs" << std::endl; 
	std::cerr << "" << std::endl; 
	std::cerr << "      filename_K.mrc            Fourier space transfer fn, as MRC density map" << std::endl; 
	std::cerr << "      filename_R.mrc            Real space point spread fn, as MRC density map" << std::endl; 
	std::cerr << "      filename_PSFres.dat       Contains values of the resolution, defined as"<< std::endl;
	std::cerr << "                                  the radius of the PSF at 1/e^2, uniformly"<< std::endl;
	std::cerr << "                                  distributed over a sphere. Can be used to" << std::endl; 
	std::cerr << "                                  to plot a histogram of directional resoluiton." << std::endl; 
	std::cerr << "      filename.log              Output values from the calculation, including the" << std::endl; 
        std::cerr << "                                  efficiency, directions of max & min res," << std::endl; 
	std::cerr << "                                  recommended tilt angles, etc."<< std::endl; 
	std::cerr << "" << std::endl; 
	std::cerr << "See README or visit www.mrc-lmb.cam.ac.uk/crusso/cryoEF/ for more information." << std::endl; 
	std::cerr << "" << std::endl; 
        return(0);
    }
    else
    {
        for (i=1; i<argc; ++i)
        {
            if (argv[i][0]=='-')
            {
                switch (argv[i][1]) //TODO: ADD ALL OPTIONS
                {
                    case 'a': 
                        da = atoi(argv[i+1]); 		//input the angular accuracy
                        break;
                    case 'b':
                        dim = atoi(argv[i+1]); 		//input the box size
                        break;
                    case 'B':
                        B = atof(argv[i+1]); 		//input B-factor
                        break;
                    case 'D':
                        D = atof(argv[i+1]); 		//input object size (Å). by default D=dim (Å)
                        nosize_flag = 0;
                        break;
                    case 'f':
                        inFile = argv[i+1]; 		//input the name (with extenstion) of the input file
                        break;
                    case 'g': 				//input symmetry
                        sym = argv[i+1];
                        sym_flag = 1;
                        break;
                    case 'm':
                        tiltmax = atof(argv[i+1]); 	//input max tilt angle
                        break;
                    case 'p':
                        dimpad = atoi(argv[i+1]); 	//input FT padding size (even!)
                        break;
                    case 'r':
                        res_FSC = atof(argv[i+1]); 	//input FSC mean resolution
                        res_flag = 1;
                        break;
                    case 't':
                        thr = atof(argv[i+1]); 		//input threshold value (observe from R.mrc)
                        break;
                    case 'W':
                        W = atof(argv[i+1]); 		//input weight factor for outlier points
                        break;
                    default:
                        std::cerr << "Unknown switch " << argv[i] << " . For help, use command with no arguments." << std::endl;
                        return 0;
                }
            }
        }
    };
    
    std::cerr << "cryoEF version 1.1.0" << std::endl;
    //std::cerr << "If useful, please cite:" << std::endl << "Naydenova, K and Russo, CJ. Measuring the effects of particle orientation" << std::endl << "to improve the efficiency of electron cryomicroscopy. Nat. Commun 8 (2017)" << std::endl << std::endl;

    std::string outFile(inFile);
    for (i=1; i<=4; i++) outFile.erase(outFile.end()-1);
    outFile.append(".log");
    
    std::ofstream logfile;
    const char* logfilename=outFile.c_str();
    logfile.open(logfilename);
    
    logfile << "----------------------------- cryoEF version 1.1.0 -----------------------------" << std::endl;
    logfile << "If useful, please cite:" << std::endl << "Naydenova, K and Russo, CJ. Measuring the effects of particle orientation" << std::endl << "to improve the efficiency of electron cryomicroscopy. Nat. Commun 8 (2017)" << std::endl << std::endl;
    time_t _tm = time(NULL);
    struct tm * curtime = localtime( &_tm );
    logfile << asctime(curtime) << std::endl;
    
    if (nosize_flag) D=dim; // by default size of object = box size (A)
    Beff = B/pow(D, 2); //convenient B-value for computation
    Apix = D/double(dim+dimpad);
    
    logfile << "-------------------------------- Program Inputs --------------------------------" << std::endl;
    logfile << "File: " << inFile << std::endl;
    logfile << "Box size: " << dim << std::endl;
    logfile << "B = " << B << " Å^2" << std::endl;
    logfile << "D = " << D << " Å" << std::endl;
    if (sym_flag) logfile << "Symmetry: " << sym << std::endl;
    if (res_flag) logfile << "FSC resolution: " << res_FSC << " Å" << std::endl;
    if (flag_outliers) logfile << "Threshold for outlier points: " << W << std::endl;

    
    int dim_rec = sqrt(1000/B)*D; //empirically good value for reasonably sized PSF in the R-space map
    char answer;
    if (fabs(float(dim_rec-dim))>0.05*dim_rec)
    {
        std::cerr << "Suggested box size (for high res calculations): " << dim_rec << std::endl;
        /*std::cout << "DO YOU WANT TO CHANGE THE BOX SIZE TO THE SUGGESTED VALUE? (Y/N) ";
        std::cin >> answer;
        if (answer=='y' || answer=='Y') dim=dim_rec; */
    }
    
    Array3D Kmap(dim, dim, dim, 0);
    Array3D Rmap(dim, dim, dim, 0);
    
    n=NoOfEntries(inFile);

    logfile << std::endl << "--------------------------------- Runtime Info ---------------------------------" << std::endl;
    
 /*****scope for handling angles and corresponding plane equations*****/
    {
        std::vector<int> phi(n);
        std::vector<int> theta(n);
        ReadAngles(inFile, n, phi, theta, da);
        logfile << "Input read from file done" << std::endl;
        
        RadixSort(phi, theta, n);
        RadixSort(theta, phi, n);
        
        //logfile << "DATA SORT DONE" << std::endl;
    
        std::vector<double> a, b, c;
        std::vector<int> bin_val;
        int size;
        if (pow(180/da+1, 2.0)<n) size=pow(180/da+1, 2.0); else size=n;
        a.reserve(size);
        b.reserve(size);
        c.reserve(size);
        bin_val.reserve(size);
    
        nviews = CompressAndCalc(theta, phi, a, b, c, bin_val, n); //find all *unique* plane coefficients
        logfile << "Calculation of planes done" << std::endl;

        if (sym_flag)
        {
            HandleSymmerty(sym, a, b, c, bin_val, nviews, logfile); //append equivalent views due to symmetry
        }
        
        nviews=a.size();
        
        std::cerr << "Estimated computation time: " << std::setprecision (2) << (double(nviews)/pow(706.0/dim, 3))/60.0 << " hours" << std::endl; //empirical observation on LMB machine
	
        for(i=0; i<nviews; ++i)
        {
            
	  //check for outliers and discard them
            
        if (!flag_outliers) CalcMap(a[i], b[i], c[i], bin_val[i], Kmap, dim, Beff); //fill in values in the K-space map
            //Beff is instead of B: downscaling B by const^2 -> worsen res *const times
        else
        {
	    double outlier = W*sqrt(n/nviews);
            if (bin_val[i] > outlier) CalcMap(a[i], b[i], c[i], bin_val[i], Kmap, dim, Beff);
        }
	  /*show progress bar:
            std::cout << i+1 << " / " << nviews << "\r";
            std::cout.flush();
       */
        }
        //std::cout << std::endl;
        
    }
    //now phi, theta, a, b, c, bin_val memory is fee
    
    for (i=1; i<=4; i++) outFile.erase(outFile.end()-1);
    outFile.append("_K.mrc");
    const char* KoutFile = outFile.c_str();
    
    NormalizeSquared(Kmap, dim);
    Print(Kmap, dim, KoutFile);
    logfile << "K-space .mrc file written" << std::endl;
    
    FindAngularDistribution(Kmap, dim, logfile);
    
    FourierTransform(Kmap, Rmap, dim, dimpad);
    Normalize(Rmap, dim);
    
    for (i=1; i<=6; i++) outFile.erase(outFile.end()-1);
    outFile.append("_R.mrc");
    const char* RoutFile = outFile.c_str();
    Print(Rmap, dim, RoutFile);
    logfile << "R-space .mrc file written" << std::endl;

    for (i=1; i<=6; i++) outFile.erase(outFile.end()-1);
    outFile.append("_PSFres.dat");
    ResolutionCalculation(n, Rmap, dim, thr, Apix, da, tiltmax, res_FSC, res_flag, logfile, outFile); //resolution assesment: mean, best, worst, variance, relative spread (see detailed description in function)
    
    logfile.close();
    std::cerr << "Finished - see .log file" << std::endl;
}




