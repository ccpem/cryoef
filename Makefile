CFLAGS = -m64

CINC=-I./lib

SRC = lib/mrc_io.c lib/mrc_utils.c lib/mrc_error.c lib/mrc_time.c \
              lib/mrc_ftns.c

RANLIB = ls

OBJS2 = $(SRC:.c=.o)

.cc.o:
	$(CC) -c $(CFLAGS) $(CINC) -o $@ $<
	$(CXX) $(CFLAGS) $(CINC) -c $<

all: libs cryoEF
	rm -f cryoEF.o

cryoEF: src/cryoEF.o
	$(CXX) -Werror -Wall -Wextra -pedantic -std=c++0x -o bin/cryoEF src/cryoEF.o -Llib -lm -lmrc -lfftw3
	
libs: lib/libmrc.a

lib/libmrc.a: $(OBJS2)
	        rm -f lib/libmrc.a
	        $(AR) cvr lib/libmrc.a $(OBJS2)
	        $(RANLIB) lib/libmrc.a

lib/mrc_io.o:	lib/mrc_types.h lib/mrc_io.h lib/mrc_io.c
lib/mrc_error.o:    lib/mrc_types.h lib/mrc_error.h lib/mrc_error.c
lib/mrc_utils.o:    lib/mrc_types.h lib/mrc_utils.h lib/mrc_utils.c
lib/mrc_time.o:     lib/mrc_types.h lib/mrc_time.h lib/mrc_time.c
lib/FindFFTValue.o: lib/FindFFTValue.c lib/fft_values.h

clean: 
	rm -f src/*.o lib/*.o core bin/cryoEF lib/libmrc.a

test:
	@echo "Beginning a test on cryoEF to check if the program compiled successfully."
	@echo "This will run cryoEF on the test data provided and will compare the result with the expected output."
	@echo "If the results are not identical, you will see a warning."
	@echo "Expected testing time: about 8 minutes."
	@-bin/cryoEF -f TestData/angles.dat -b 128 -B 160 -D 128 > TestData/test.log 2>&1
	@sed '6d' TestData/angles.log > TestData/angles_NoTimeStamp.tmp
	@sed '6d' TestData/SampleOutput/angles.log > TestData/SampleOutput/angles_NoTimeStamp.tmp
	@if diff TestData/angles_NoTimeStamp.tmp TestData/SampleOutput/angles_NoTimeStamp.tmp; \
	then \
		echo "Success"; \
	else \
		echo "cryoEF output differs from expected, see differences listed above."; \
		echo "Small discrepancies between the outputs might be due to different precision on different systems."; \
		echo "Use this information to judge if cryoEF is working properly."; \
	fi
	@rm -f TestData/*.log TestData/*.mrc TestData/angles_PSFres.dat TestData/*.tmp TestData/SampleOutput/*.tmp
