#ifndef MRC_IO
#define MRC_IO


int mrc_open( const char *file_name, int flags );
int mrc_create_file(int mrc_id,  const char *file_name );
int mrc_new( void );
int mrc_close( int mrc_id );
int mrc_rewind( int mrc_id );
int mrc_dup( int mrc_id );

int mrc_get_dimen( int mrc_id, int *nx, int *ny, int *nz );
int mrc_set_dimen( int mrc_id, int nx, int ny, int nz );
int mrc_get_mdimen( int mrc_id, int *mx, int *my, int *mz );
int mrc_set_mdimen( int mrc_id, int mx, int my, int mz );
int mrc_get_nstart( int mrc_id, int *nxstart, int *nystart, int *nzstart );

int mrc_set_type( int mrc_id, int type );
int mrc_set_type_uchar( int mrc_id );
int mrc_set_type_short( int mrc_id );
int mrc_set_type_ushort( int mrc_id );
int mrc_set_type_float( int mrc_id );
int mrc_set_type_double( int mrc_id );

int mrc_write_header( int mrc_id );
int mrc_print_header( int mrc_id );

int mrc_get_max_min_mean_rms( int mrc_id, double  *dmax, double *min, double *dmean, double *rms);
int mrc_set_max_min_mean_rms( int mrc_id, double  dmax, double min, double dmean, double rms);
int mrc_write_max_min_mean_rms( int mrc_id, double  dmax, double min, double dmean, double rms);

size_t mrc_get_data_size( int mrc_id );

int mrc_write_line( int mrc_id, const void *buf, size_t len );

/* int mrc_write_block( int mrc_id, const unsigned char *buf, size_t lda, int n, int m, int ipos, int jpos ); */

int mrc_write_block( int mrc_id, const unsigned char *buf, size_t lda, int n, int m, int ipos, int jpos, 
			size_t data_size );
int mrc_write_data( int mrc_id, void *buf, size_t nbytes );
int mrc_read_line( int mrc_id, void *buf );

int mrc_read_block( int mrc_id, void *buf, size_t len );

int mrc_get_origin( int mrc_id, double *origin );
int mrc_set_origin( int mrc_id, double *origin );

int mrc_add_label( int mrc_id, const char *label );

int mrc_get_label_number( int mrc_id );

char *mrc_get_label( int mrc_id, int number );

int mrc_get_resolution( int mrc_id, double *res );

int mrc_get_fp( int mrc_id );

int mrc_test_mrc_map( const char *filename );

int mrc_save_data( char *filename, int type, void *x, int lda, int nx, int ny );


void *mrc_get_header( int mrc_id );

int mrc_get_cell_dimen( int mrc_id, double *cella );
int mrc_set_cell_dimen( int mrc_id, double *cella );

int mrc_read_page_to_double(int mrc_id, size_t data_size,
		double *dbuf, size_t lda, int nx, int ny,
		unsigned char *line_buf );

int mrc_read_page_to_float(int mrc_id, size_t data_size,
		float *fbuf, size_t lda, int nx, int ny,
		unsigned char *line_buf );


#endif
