#ifndef MRC_UTILS
#define MRC_UTILS

#include "mrc_types.h"

void 	mrc_swap_header( MRC_Header *header );
void 	swap_four( void  *in );
void 	swap_eight( void  *in );
char	*mrc_gen_filename( const char *name, const char *suffix );
void 	mrc_print_program_name( char *s );
void 	mrc_print_args( int argc, char *argv[] );
void 	mrc_print_line( void );
char	*mrc_get_time( void );


#endif
