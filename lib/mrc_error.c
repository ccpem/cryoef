#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>



void mrc_error( const char *fmt, ... )
{
	va_list	ap;

	fflush(stdout);
	fprintf(stderr,"ERROR: ");

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fprintf(stderr,"\n");
	fflush(stderr);
	exit(1);

}
void mrc_warning( const char *fmt, ... )
{
	va_list	ap;

	fflush(stdout);
	fprintf(stderr, 
"\n************************************************************************\n");
	fprintf(stderr,"WARNING: ");

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fprintf(stderr,"\n");
	fprintf(stderr,
  "************************************************************************\n");
	fflush(stderr);

}
