#ifndef MRC_HEADER
#define MRC_HEADER

#ifdef __MINGW32__

#include "../include/getopt.h"
#define HUGE 3.40282347e+38F

#undef HAVE_FSYNC
#define HAVE_FSYNC 1
#define fsync(__fd) ((FlushFileBuffers ((HANDLE) _get_osfhandle (__fd))) ? 0 : -1)



#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mrc_error.h"
#include "mrc_io.h"
#include "mrc_utils.h"
#include "mrc_time.h"
#include "mrc_ftns.h"
#include "mrc_interp_ftns.h"



#include "mrc_random.h"

double	CalcWavelength( double voltage );

int FindFFTValue( int n );

int GenGroupMatrices( char *symm_name, int inv, double **matrix );


#endif
