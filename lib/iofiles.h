extern "C"
{
#include "mrc.h"
}

/*****Function to find number of input datapoints*****/
int NoOfEntries(const char* inFile)
{
    int count=0;
    std::string line;
    std::ifstream is(inFile);
    while(getline(is,line)) ++count;
    is.close();
    return count;
}

/*****Function to read and round up angles from 2-column text file*****/
void ReadAngles (const char* inFile, int &n, std::vector<int>& phi, std::vector<int>& theta, int da)
{
    int j=0;
    double phi_o, theta_o;
    std::ifstream is(inFile);
    
    while(is>>phi_o>>theta_o)
    {
        theta[j]=Quantize(theta_o,da);
        phi[j]=Quantize(phi_o,da);
	
	if (theta[j]>180)
	  {
	    theta[j]=theta[j]-180;
	    phi[j]=phi[j]-180;
	  }
        if (phi[j]<0)
        {
            phi[j]=phi[j]+180;
            theta[j]=180-theta[j];
        }
	
	if (phi[j]>180)
	  {
	    phi[j]=phi[j]-180;
	    theta[j]=180-theta[j];
	  }

        ++j;
    }
    
    is.close();
}

/*****Write to .mrc file*****/
void Print (Array3D& dmap, int dim, const char* outFile)
{
    int pad=10;
    int ldim=dim+pad;
    
    float *array=NULL;
    array=new float[ldim*dim*dim];
    char *outFile_ed;
    outFile_ed=const_cast<char *>(outFile);
    
    int i,j,k;
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i) array[i+ldim*(j+k*dim)]=dmap(i,j,k);
    
    int mrcgen;
    mrcgen=mrc_save_map(outFile_ed, sizeof(float), array, ldim, dim, dim, dim);
    delete [] array;
    
    
}


/*****Read from .mrc file*****/
void ReadMRC (Array3D& dmap, int dim, const char* inFile)
{
    int i, j, k;
    std::ifstream is(inFile, std::ios::in|std::ios::binary);
    MRC_Header a;
    is.read(reinterpret_cast<char *>(&a),sizeof(MRC_Header));
    if (a.nx!=dim) std::cerr << "WARNING: TRUE DIMENSION OF MAP IS: " << a.nx << ". PLEASE RESTART WITH CORRECT DIMENSION!" << std::endl;
    //First 10 values are integers: nc, nr, ns, mode, ncstart, nrstart, nsstart, nx, ny, nz
    //Next 12 entries (10 to 22) are floats: xlength, ylength, zlength, alpha, beta, gamma, mapc, mapr, maps (the first three are cell dimensions)
    float b=0;
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i)
    {
        is.read(reinterpret_cast<char *>(&b),sizeof(float));
        dmap(i,j,k)=b;
    }
    is.close();
    
}
