/*****Symmetries defined as in XMIPP*****/
const int pg_CN=1;
const int pg_CI=2;
const int pg_CS=3;
const int pg_CNH=4;
const int pg_CNV=5;
const int pg_SN=6;
const int pg_DN=7;
const int pg_DNV=8;
const int pg_DNH=9;
const int pg_T=10;
const int pg_TD=11;
const int pg_TH=12;
const int pg_O=13;
const int pg_OH=14;
const int pg_I=15;
const int pg_I1=16;
const int pg_I2=17;
const int pg_I3=18;
const int pg_I4=19;
const int pg_I5=20;
const int pg_IH=21;
const int pg_I1H=22;
const int pg_I2H=23;
const int pg_I3H=24;
const int pg_I4H=25;
const int pg_I5H=26;

/*****Adding equivalent (symmetric) views*****/
void AddPoints (Matrix3x3& R, std::vector<double>& a, std::vector<double>& b, std::vector<double>& c, std::vector<int>& bin_val, int nviews)
{
    int i;
    double aeq, beq, ceq;
    
    for (i=0; i<nviews; ++i)
    {
        Transform(R, a[i], b[i], c[i], aeq, beq, ceq);
        a.push_back(aeq);
        b.push_back(beq);
        c.push_back(ceq);
        bin_val.push_back(bin_val[i]);
    }
    
}

/*****Reflection matrix calculation from given mirror plane (nx,ny,nz)*****/
void GetReflMatrix (Matrix3x3& M, double nx, double ny, double nz)
{
    M(0,0)=1-2*nx*nx;
    M(0,1)=0-2*nx*ny;
    M(0,2)=0-2*nx*nz;
    M(1,0)=M(0,1);
    M(1,1)=1-2*ny*ny;
    M(1,2)=0-2*ny*nz;
    M(2,0)=M(0,2);
    M(2,1)=M(1,2);
    M(2,2)=1-2*nz*nz;
}

/****Rotation matrix calculation from given axis (u,v,w) and angle*****/
void GetRotGenMatrix (Matrix3x3& G, double u, double v, double w, double alpha)
{
    double cosa=cosd(alpha);
    double sina=sind(alpha);
    G(0,0)=u*u+(1-u*u)*cosa;
    G(0,1)=u*v*(1-cosa)-w*sina;
    G(0,2)=u*w*(1-cosa)+v*sina;
    G(1,0)=u*v*(1-cosa)+w*sina;
    G(1,1)=v*v+(1-v*v)*cosa;
    G(1,2)=v*w*(1-cosa)-u*sina;
    G(2,0)=u*w*(1-cosa)-v*sina;
    G(2,1)=v*w*(1-cosa)+u*sina;
    G(2,2)=w*w+(1-w*w)*cosa;
    
    for (int i=0; i<dm; i++)
        for (int j=0; j<dm; j++)
            if (fabs(G(i,j))<RoundingErr) G(i,j)=0;
}

/*****Function for scanning the array of group elements*****/
bool NotFound (Matrix3x3& R, std::vector<Matrix3x3>& GrEl, int n)
{
    for (int i=0; i<n; ++i)
        if (GrEl[i]==R) return 0;
    
    return 1;
}

/*****Function for generating group elements as random combinations of already found ones*****/
Matrix3x3 Mulligan (std::vector<Matrix3x3>& GrEl, int n)
{
    srand (time(NULL));
    int i=rand()%(n-1)+1;
    int j=rand()%(n-1)+1;
    return GrEl[i]*GrEl[j];
}

/*****Symmetry input handling*****/
bool IsSymmetryGroup(const char* sym, int &pgGroup, int &pgOrder)
{
    char G1,G2,G3,G4;
    char auxChar[3];
    //each case check length, check first letter, second, is number
    
    int mySize=strlen(sym);
    bool return_true;
    return_true=false;
    auxChar[2]='\0';
    //size may be 4 because n may be a 2 digit number
    if(mySize>4 || mySize<1)
    {
        pgGroup=-1;
        pgOrder=-1;
        return false;
    }
    
    //get the group name character by character
    G1=toupper(sym[0]);
    G2=toupper(sym[1]);
    if (mySize > 2)
    {   G3=toupper(sym[2]);
        if(mySize > 3)
            G4=toupper(sym[3]);
    }
    else
        G4='\0';
    
    
    //CN
    if (mySize==2 && G1=='C' && isdigit(G2))
    {
        pgGroup=pg_CN;
        pgOrder=int(G2)-48;
        return_true=true;
    }
    if (mySize==3 && G1=='C' && isdigit(G2) && isdigit(G3))
    {
        pgGroup=pg_CN;
        auxChar[0]=G2;
        auxChar[1]=G3;
        pgOrder=atoi(auxChar);
        return_true=true;
    }
    
    //CI
    else if (mySize==2 && G1=='C' && G2=='I')
    {
        pgGroup=pg_CI;
        pgOrder=-1;
        return_true=true;
    }
    
    //CS
    else if (mySize==2 && G1=='C' && G2=='S')
    {
        pgGroup=pg_CS;
        pgOrder=-1;
        return_true=true;
    }
    
    //CNH
    else if (mySize==3 && G1=='C' && isdigit(G2) && G3=='H')
    {
        pgGroup=pg_CNH;
        pgOrder=int(G2)-48;
        return_true=true;
    }
    else if (mySize==4 && G1=='C' && isdigit(G2) && isdigit(G3) && G4=='H')
    {
        pgGroup=pg_CNH;
        auxChar[0]=G2;
        auxChar[1]=G3;
        pgOrder=atoi(auxChar);
        return_true=true;
    }
    
    //CNV
    else if (mySize==3 && G1=='C' && isdigit(G2) && G3=='V')
    {
        pgGroup=pg_CNV;
        pgOrder=int(G2)-48;
        return_true=true;
    }
    else if (mySize==4 && G1=='C' && isdigit(G2) && isdigit(G3) && G4=='V')
    {
        pgGroup=pg_CNV;
        auxChar[0]=G2;
        auxChar[1]=G3;
        pgOrder=atoi(auxChar);
        return_true=true;
    }
    
    //SN
    else if (mySize==2 && G1=='S' && isdigit(G2) )
    {
        pgGroup=pg_SN;
        pgOrder=int(G2)-48;
        return_true=true;
    }
    else if (mySize==3 && G1=='S' && isdigit(G2) && isdigit(G3) )
    {
        pgGroup=pg_SN;
        auxChar[0]=G2;
        auxChar[1]=G3;
        pgOrder=atoi(auxChar);
        return_true=true;
    }
    
    //DN
    else if (mySize==2 && G1=='D' && isdigit(G2) )
    {
        pgGroup=pg_DN;
        pgOrder=int(G2)-48;
        return_true=true;
    }
    if (mySize==3 && G1=='D' && isdigit(G2) && isdigit(G3))
    {
        pgGroup=pg_DN;
        auxChar[0]=G2;
        auxChar[1]=G3;
        pgOrder=atoi(auxChar);
        return_true=true;
    }
    
    //DNV
    else if (mySize==3 && G1=='D' && isdigit(G2) && G3=='V')
    {
        pgGroup=pg_DNV;
        pgOrder=int(G2)-48;
        return_true=true;
    }
    else if (mySize==4 && G1=='D' && isdigit(G2) && isdigit(G3) && G4=='V')
    {
        pgGroup=pg_DNV;
        auxChar[0]=G2;
        auxChar[1]=G3;
        pgOrder=atoi(auxChar);
        return_true=true;
    }
    
    //DNH
    else if (mySize==3 && G1=='D' && isdigit(G2) && G3=='H')
    {
        pgGroup=pg_DNH;
        pgOrder=int(G2)-48;
        return_true=true;
    }
    else if (mySize==4 && G1=='D' && isdigit(G2) && isdigit(G3) && G4=='H')
    {
        pgGroup=pg_DNH;
        auxChar[0]=G2;
        auxChar[1]=G3;
        pgOrder=atoi(auxChar);
        return_true=true;
    }
    
    //T
    else if (mySize==1 && G1=='T')
    {
        pgGroup=pg_T;
        pgOrder=-1;
        return_true=true;
    }
    
    //TD
    else if (mySize==2 && G1=='T' && G2=='D')
    {
        pgGroup=pg_TD;
        pgOrder=-1;
        return_true=true;
    }
    
    //TH
    else if (mySize==2 && G1=='T' && G2=='H')
    {
        pgGroup=pg_TH;
        pgOrder=-1;
        return_true=true;
    }
    
    //O
    else if (mySize==1 && G1=='O')
    {
        pgGroup=pg_O;
        pgOrder=-1;
        return_true=true;
    }
    
    //OH
    else if (mySize==2 && G1=='O'&& G2=='H')
    {
        pgGroup=pg_OH;
        pgOrder=-1;
        return_true=true;
    }
    
    //I
    else if (mySize==1 && G1=='I')
    {
        pgGroup=pg_I;
        pgOrder=-1;
        return_true=true;
    }
    
    //I1
    else if (mySize==2 && G1=='I'&& G2=='1')
    {
        pgGroup=pg_I1;
        pgOrder=-1;
        return_true=true;
    }
    
    //I2
    else if (mySize==2 && G1=='I'&& G2=='2')
    {
        pgGroup=pg_I2;
        pgOrder=-1;
        return_true=true;
    }
    
    //I3
    else if (mySize==2 && G1=='I'&& G2=='3')
    {
        pgGroup=pg_I3;
        pgOrder=-1;
        return_true=true;
    }
    
    //I4
    else if (mySize==2 && G1=='I'&& G2=='4')
    {
        pgGroup=pg_I4;
        pgOrder=-1;
        return_true=true;
    }
    
    //I5
    else if (mySize==2 && G1=='I'&& G2=='5')
    {
        pgGroup=pg_I5;
        pgOrder=-1;
        return_true=true;
    }
    
    //IH
    else if (mySize==2 && G1=='I'&& G2=='H')
    {
        pgGroup=pg_IH;
        pgOrder=-1;
        return_true=true;
    }
    
    //I1H
    else if (mySize==3 && G1=='I'&& G2=='1'&& G3=='H')
    {
        pgGroup=pg_I1H;
        pgOrder=-1;
        return_true=true;
    }
    
    //I2H
    else if (mySize==3 && G1=='I'&& G2=='2'&& G3=='H')
    {
        pgGroup=pg_I2H;
        pgOrder=-1;
        return_true=true;
    }
    
    //I3H
    else if (mySize==3 && G1=='I'&& G2=='3'&& G3=='H')
    {
        pgGroup=pg_I3H;
        pgOrder=-1;
        return_true=true;
    }
    
    //I4H
    else if (mySize==3 && G1=='I'&& G2=='4'&& G3=='H')
    {
        pgGroup=pg_I4H;
        pgOrder=-1;
        return_true=true;
    }
    
    //I5H
    else if (mySize==3 && G1=='I'&& G2=='5'&& G3=='H')
    {
        pgGroup=pg_I5H;
        pgOrder=-1;
        return_true=true;
    }
    
    return return_true;
}

/*****Function for generating and performing the symmetry transformations for the given symmerty group*****/
void SymmetryMatrices(int pgGroup, int pgOrder, std::vector<double>& a, std::vector<double>& b, std::vector<double>& c, std::vector<int>& bin_val, int nviews)
{
    int i, j, k, m;
    int rot_elements;
    int rotations=0;
    bool reflection=0;
    bool inversion=1;
    double u[3], v[3], w[3], order[3]; //max 3 different rotation generators one group
    double alpha;
    double nx, ny, nz;
    nx=0;
    ny=0;
    nz=0;
    for (i=0; i<3; ++i)
    {
        u[i]=0;
        v[i]=0;
        w[i]=0;
        order[i]=0;
    }
    
    if (pgGroup == pg_CN)
    {
        w[0]=1;
        order[0]=pgOrder;
        //std::cout << "rot_axis " << pgOrder << " 0 0 1" << std::endl;
        rotations=1;
	if (pgOrder == 1) rotations=0; //To handle C1 case
        rot_elements=pgOrder;
    }
    
    else if (pgGroup == pg_CI)
    {
        //std::cout << "inversion";
        inversion=1;
        //Inversion handling for our purpose: every entry gets n x 2 - irrelevant
    }
    
    else if (pgGroup == pg_CS)
    {
        nz=1;
        //std::cout << "mirror_plane 0 0 1" << std::endl;
        reflection=1;
        rot_elements=1;
    }
    
    else if (pgGroup == pg_CNV)
    {
        w[0]=1;
        order[0]=pgOrder;
        ny=1;
        //std::cout << "rot_axis " << pgOrder << "  0 0 1" << std::endl;;
        //std::cout << "mirror_plane 0 1 0" << std::endl;
        rotations=1;
	if (pgOrder == 1) rotations=0; //To handle C1V case
        reflection=1;
        rot_elements=pgOrder;
        
    }
    
    else if (pgGroup == pg_CNH)
    {
        w[0]=1;
        order[0]=pgOrder;
        nz=1;
        //std::cout << "rot_axis " << pgOrder << "  0 0 1" << std::endl;
        //std::cout << "mirror_plane 0 0 1" << std::endl;
        rotations=1;
	if (pgOrder == 1) rotations=0; //To handle C1V case
        reflection=1;
        rot_elements=pgOrder;
    }
    
    else if (pgGroup == pg_SN)
    {
        if(pgOrder%2!=0)
        {
            std::cerr << "Error: Order for Sn group must be even" << std::endl;
            exit(0);
        }
        
        order[0]=pgOrder/2;
        w[0]=1;
        //std::cout << "rot_axis " << pgOrder << " 0 0 1" << std::endl;
        //std::cout << "inversion" << std::endl;
        rotations=1;
        inversion=1;
        rot_elements=order[0];
    }
    
    else if (pgGroup == pg_DN)
    {
	u[0]=1;
        order[0]=2;
        //std::cout << "rot_axis 2  1 0 0" << std::endl;

	if (pgOrder != 1)
	{
        	w[1]=1;
        	order[1]=pgOrder;
        	//std::cout << "rot_axis " << pgOrder << "  0 0 1" << std::endl;
        	rotations=2;
	}
	else rotations = 1;
	rot_elements=2*pgOrder;
	
    }
    
    else if (pgGroup == pg_DNV)
    {
        u[0]=1;
        order[0]=2;
        //std::cout << "rot_axis 2  1 0 0" << std::endl;
        nx=1;
        //std::cout << "mirror_plane 1 0 0" << std::endl;
        if (pgOrder != 1)
	{
        	w[1]=1;
        	order[1]=pgOrder;
        	//std::cout << "rot_axis " << pgOrder << "  0 0 1" << std::endl;
        	rotations=2;
	}
        else rotations = 1;
        reflection=1;
        rot_elements=2*pgOrder;
    }
    
    else if (pgGroup == pg_DNH)
    {
        u[0]=1;
        order[0]=2;
        //std::cout << "rot_axis 2  1 0 0" << std::endl;
        nz=1;
        //std::cout << "mirror_plane 0 0 1" << std::endl;
 	if (pgOrder != 1)
	{
        	w[1]=1;
        	order[1]=pgOrder;
        	//std::cout << "rot_axis " << pgOrder << "  0 0 1" << std::endl;
        	rotations=2;
	}
        else rotations = 1;
        reflection=1;
        rot_elements=2*pgOrder;
        
    }
    
    else if (pgGroup == pg_T)
    {
        w[0]=1;
        order[0]=3;
        //std::cout << "rot_axis 3  0 0 1" << std::endl;
        v[1]=0.816496;
        w[1]=0.577350;
        order[1]=2;
        //std::cout << "rot_axis 2  0 0.816496 0.577350" << std::endl;
        rotations=2;
        rot_elements=12;
    }
    
    else if (pgGroup == pg_TD)
    {
        w[0]=1;
        order[0]=3;
        //std::cout << "rot_axis 3  0 0 1" << std::endl;
        v[1]=0.816496;
        w[1]=0.577350;
        order[1]=2;
        //std::cout << "rot_axis 2  0 0.816496 0.577350" << std::endl;
        nx=0.5;
        ny=0.8660254;
        //std::cout << "mirror_plane 0.5 0.8660254 0.0000000" << std::endl;
        rotations=2;
        reflection=1;
        rot_elements=12;
    }
    
    else if (pgGroup == pg_TH)
    {
        w[0]=1;
        order[0]=3;
        //std::cout << "rot_axis 3  0 0 1" << std::endl;
        v[1]=-0.816496;
        w[1]=-0.577350;
        order[1]=2;
        //std::cout << "rot_axis 2  0 -0.816496 -0.577350" << std::endl;
        //std::cout << "inversion" << std::endl;
        rotations=2;
        inversion=1;
        rot_elements=24;
    }
    
    else if (pgGroup == pg_O)
    {
        u[0]=0.5773502;
        v[0]=0.5773502;
        w[0]=0.5773502;
        order[0]=3;
        //std::cout << "rot_axis 3  0.5773502  0.5773502 0.5773502" << std::endl;
        w[1]=1;
        order[1]=4;
        //std::cout << "rot_axis 4  0 0 1" << std::endl;
        rotations=2;
        rot_elements=24;
    }
    
    else if (pgGroup == pg_OH)
    {
        u[0]=0.5773502;
        v[0]=0.5773502;
        w[0]=0.5773502;
        order[0]=3;
        //std::cout << "rot_axis 3  0.5773502  0.5773502 0.5773502" << std::endl;
        w[1]=1;
        order[1]=4;
        //std::cout << "rot_axis 4  0 0 1" << std::endl;
        ny=1;
        nz=1;
        //std::cout << "mirror_plane 0 1 1" << std::endl;
        rotations=2;
        reflection=1;
        rot_elements=24;
    }
    
    else if (pgGroup == pg_I || pgGroup == pg_I2)
    {
        w[0]=1;
        order[0]=2;
        //std::cout << "rot_axis 2  0 0 1" << std::endl;
        u[1]=0.525731114;
        w[1]=0.850650807;
        order[1]=5;
        //std::cout << "rot_axis 5  0.525731114 0 0.850650807" << std::endl;
        v[2]=0.356822076;
        w[2]=0.934172364;
        order[2]=3;
        //std::cout << "rot_axis 3  0 0.356822076 0.934172364" << std::endl;
        rotations=3;
        rot_elements=60;
    }
    
    else if (pgGroup == pg_I1)
    {
        u[0]=1;
        order[0]=2;
        //std::cout << "rot_axis 2  1 0 0" << std::endl;
        w[1]=-0.5257311142635;
        u[1]=0.85065080702670;
        order[1]=5;
        //std::cout << "rot_axis 5  0.85065080702670 0 -0.5257311142635" << std::endl;
        v[2]=0.3568220765;
        u[2]=0.9341723640;
        order[2]=3;
        //std::cout << "rot_axis 3  0.9341723640 0.3568220765 0" << std::endl;
        rotations=3;
        rot_elements=60;
    }
    
    else if (pgGroup == pg_I3)
    {
        u[0]=-0.5257311142635;
        w[0]=0.85065080702670;
        order[0]=2;
        //std::cout << "rot_axis 2  -0.5257311143 0 0.8506508070" << std::endl;
        w[1]=1;
        order[1]=5;
        //std::cout << "rot_axis 5  0 0 1" << std::endl;
        u[2]=-0.4911234778630044;
        v[2]=0.3568220764705179;
        w[2]=0.7946544753759428;
        order[2]=3;
        //std::cout << "rot_axis 3  -0.4911234778630044 0.3568220764705179 0.7946544753759428" << std::endl;
        rotations=3;
        rot_elements=60;
    }
    
    else if (pgGroup == pg_I4)
    {
        u[0]=0.5257311142635;
        w[0]=0.85065080702670;
        order[0]=2;
        //std::cout << "rot_axis 2  0.5257311143 0 0.8506508070" << std::endl;
        u[1]=0.8944271932547096;
        w[1]=0.4472135909903704;
        order[1]=5;
        //std::cout << "rot_axis 5  0.8944271932547096 0 0.4472135909903704" << std::endl;
        u[2]=0.4911234778630044;
        v[2]=0.3568220764705179;
        w[2]=0.7946544753759428;
        order[2]=3;
        //std::cout << "rot_axis 3  0.4911234778630044 0.3568220764705179 0.7946544753759428" << std::endl;
        rotations=3;
        rot_elements=60;
    }
    
    else if (pgGroup == pg_I5)
    {
        std::cerr << "Error: Symmetry I5 not implemented" << std::endl;
        exit(0);
    }
    
    else if (pgGroup == pg_IH || pgGroup == pg_I2H)
    {
        w[0]=1;
        order[0]=2;
        //std::cout << "rot_axis 2  0 0 1" << std::endl;
        u[1]=0.525731114;
        w[1]=0.850650807;
        order[1]=5;
        //std::cout << "rot_axis 5  0.525731114 0 0.850650807" << std::endl;
        v[2]=0.3568220765;
        w[2]=0.9341723640;
        order[2]=3;
        //std::cout << "rot_axis 3  0 0.3568220765 0.9341723640" << std::endl;
        nx=1;
        //std::cout << "mirror_plane 1 0 0" << std::endl;
        rotations=3;
        reflection=1;
        rot_elements=60;
    }
    
    else if (pgGroup == pg_I1H)
    {
        u[0]=1;
        order[0]=2;
        //std::cout << "rot_axis 2  1 0 0" << std::endl;
        w[1]=-0.5257311142635;
        u[1]=0.85065080702670;
        order[1]=5;
        //std::cout << "rot_axis 5  0.85065080702670 0 -0.5257311142635" << std::endl;
        v[2]=0.3568220765;
        u[2]=0.9341723640;
        order[2]=3;
        //std::cout << "rot_axis 3  0.9341723640 0.3568220765 0" << std::endl;
        nz=-1;
        //std::cout << "mirror_plane 0 0 -1" << std::endl;
        rotations=3;
        reflection=1;
        rot_elements=60;
    }
    
    else if (pgGroup == pg_I3H)
    {
        u[0]=-0.5257311143;
        w[0]=0.8506508070;
        order[0]=2;
        //std::cout << "rot_axis 2  -0.5257311143 0 0.8506508070" << std::endl;
        w[1]=1;
        order[1]=5;
        //std::cout << "rot_axis 5  0 0 1" << std::endl;
        u[2]=-0.4911234778630044;
        v[2]=0.3568220764705179;
        w[2]=0.7946544753759428;
        order[2]=3;
        //std::cout << "rot_axis 3  -0.4911234778630044 0.3568220764705179 0.7946544753759428" << std::endl;
        nx=0.850650807;
        nz=0.525731114;
        //std::cout << "mirror_plane 0.850650807 0  0.525731114" << std::endl;
        rotations=3;
        reflection=1;
        rot_elements=60;
    }
    
    else if (pgGroup == pg_I4H)
    {
        u[0]=0.5257311143;
        w[0]=0.8506508070;
        order[0]=2;
        //std::cout << "rot_axis 2  0.5257311143 0 0.8506508070" << std::endl;
        u[1]=0.8944271932547096;
        w[1]=0.4472135909903704;
        order[1]=5;
        //std::cout << "rot_axis 5  0.8944271932547096 0 0.4472135909903704" << std::endl;
        u[2]=0.4911234778630044;
        v[2]=0.3568220764705179;
        w[2]=0.7946544753759428;
        order[2]=3;
        //std::cout << "rot_axis 3  0.4911234778630044 0.3568220764705179 0.7946544753759428" << std::endl;
        nx=0.850650807;
        nz=-0.525731114;
        //std::cout << "mirror_plane 0.850650807 0  -0.525731114" << std::endl;
        rotations=3;
        reflection=1;
        rot_elements=60;
    }
    
    else if (pgGroup == pg_I5H)
    {
        std::cerr << "Error: Symmetry I5H not implemented" << std::endl;
        exit(0);
    }
    
    else
    {
        std::cerr<< "Error: Symmetry unknown" << std::endl;
        exit(0);
    }
    
    Matrix3x3 R, M, G[3], RM;
    R(0,0)=1;
    R(1,1)=1;
    R(2,2)=1;
    
    
    int index=0;
    std::vector<Matrix3x3> GrEl(rot_elements); //here group order is for the corrsep group without considering reflections
    GrEl[0] = R;
    ++index;
    
    //std::cout << "PERFORMING SYMMETRY OPERATIONS..." << std::endl;
    
    if (reflection)
    {
        GetReflMatrix(M, nx, ny, nz);
        //M.print();
        AddPoints(M, a, b, c, bin_val, nviews);
    }
    
    for (k=0; k<rotations; ++k)
    {
        alpha = 360.0/double(order[k]);
        GetRotGenMatrix(G[k], u[k], v[k], w[k], alpha);
        GrEl[index] = G[k];
        ++index;
        AddPoints(G[k], a, b, c, bin_val, nviews);
        if (reflection)
        {
            RM = G[k]*M;
            AddPoints(RM, a, b, c, bin_val, nviews);
        }
        
    }
    
    switch (rotations)
    {
        case 0:
            break;
        case 1:
            for (i=2; i<order[0]; ++i)
            {
                R = G[0].topower(i);
                AddPoints(R, a, b, c, bin_val, nviews);
                if (reflection)
                {
                    RM = R*M;
                    AddPoints(RM, a, b, c, bin_val, nviews);
                }
                
            }
            break;
            
        case 2: case 3:
            while (index < rot_elements)
            {
                R = Mulligan(GrEl, index);
                if(NotFound(R, GrEl, index))
                {
                    AddPoints(R, a, b, c, bin_val, nviews);
                    GrEl[index] = R;
                    index++;
                }
                if (reflection)
                {
                    RM = R*M;
                    AddPoints(RM, a, b, c, bin_val, nviews);
                }
                
            }
            break;
    }
}
