#ifndef MRC_ERROR
#define MRC_ERROR

void mrc_error( const char *fmt, ... );
void mrc_warning( const char *fmt, ... );


#endif
