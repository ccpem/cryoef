#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#ifdef __MINGW32__
#else
#include <libgen.h>
#endif

#define BUFLEN			2048



#include "mrc_types.h"
#include "mrc_utils.h"

void swap_eight( void  *n )
{
	unsigned char cbuf[8];
	unsigned char *cptr;


	cptr = (unsigned char *)n;
	cbuf[0] = cptr[0];
	cbuf[1] = cptr[1];
	cbuf[2] = cptr[2];
	cbuf[3] = cptr[3];
	cbuf[4] = cptr[4];
	cbuf[5] = cptr[5];
	cbuf[6] = cptr[6];
	cbuf[7] = cptr[7];

	cptr = (unsigned char *)n;
	cptr[0] = cbuf[7];
	cptr[1] = cbuf[6];
	cptr[2] = cbuf[5];
	cptr[3] = cbuf[4];
	cptr[4] = cbuf[3];
	cptr[5] = cbuf[2];
	cptr[6] = cbuf[1];
	cptr[7] = cbuf[0];

}

void swap_four( void  *n )
{
	unsigned char cbuf[4];
	unsigned char *cptr;

	cptr = (unsigned char *)n;
	cbuf[0] = cptr[0];
	cbuf[1] = cptr[1];
	cbuf[2] = cptr[2];
	cbuf[3] = cptr[3];

	cptr = (unsigned char *)n;
	cptr[0] = cbuf[3];
	cptr[1] = cbuf[2];
	cptr[2] = cbuf[1];
	cptr[3] = cbuf[0];
}

void cnvtint( void *buf, int *nx )
{
	unsigned char *iptr = (unsigned char *)buf;
	unsigned char *optr = (unsigned char *)nx;

	optr[0] = iptr[0];
	optr[1] = iptr[1];
	optr[2] = iptr[2];
	optr[3] = iptr[3];
	
}


/*
 * mrc_swap_header:
 * WARNING::: More bits of the header might need to be swapped 
 */

void mrc_swap_header( MRC_Header *header )
{
	swap_four( (void *)&header->mode );

	swap_four( (void *)&header->nx );
	swap_four( (void *)&header->ny );
	swap_four( (void *)&header->nz );

	swap_four( (void *)&header->mx );
	swap_four( (void *)&header->my );
	swap_four( (void *)&header->mz );


	swap_four( (void *)&header->nxstart );
	swap_four( (void *)&header->nystart );
	swap_four( (void *)&header->nzstart );

	swap_four( (void *)&header->ispg );
	swap_four( (void *)&header->nsymbt );

	swap_four( (void *)&header->dmin );
	swap_four( (void *)&header->dmax );
	swap_four( (void *)&header->dmean );

	swap_four( (void *)&header->cella[0] );
	swap_four( (void *)&header->cella[0] );
	swap_four( (void *)&header->cella[0] );

	swap_four( (void *)&header->cellb[0] );
	swap_four( (void *)&header->cellb[0] );
	swap_four( (void *)&header->cellb[0] );

	swap_four( (void *)&header->rms );
	swap_four( (void *)&header->nlabel );

	swap_four( (void *)&header->mapc );
	swap_four( (void *)&header->mapr );
	swap_four( (void *)&header->maps );

	swap_four( (void *)&header->ispg );
	swap_four( (void *)&header->nsymbt );
}


char *mrc_gen_filename( const char *filename, const char *suffix )
{
	size_t	len, llen, suffix_len;
	char	*name;
	char	*tstring;
	char	*tcptr;

	if ( suffix == NULL ){
		suffix_len = 0;
	} else {
		suffix_len  = strlen(suffix);
	}

	len = strlen(filename);
	tstring = (char *)malloc(len+1);
	if ( tstring == NULL ){
		return NULL;
	}
	strncpy( tstring, filename, len );
	tstring[len] = '\0';
#ifdef __MINGW32__
	tcptr = tstring;
#else
	tcptr = (char *)basename( tstring );
#endif
	len = strlen(tcptr);
	llen = len + suffix_len + 2;
	name = (char *)malloc(llen);
	if ( name == NULL ){
		return NULL;
	}
	strncpy( name, tcptr, len );
 	name[len] = '\0';

//	printf("GOTCHA START LEN %lu \n", len );
	while( len > 0 ){
		len--;
		if ( tcptr[len] == '.') {
			tcptr[len] = '\0';
			break;
		}
	}
//	printf("GOTCHA LEN %lu NAME : %s \n", len, &tcptr[0] );

	if ( len > 0 ){
		strncpy( name, &tcptr[0] , len+1  );
	}
//	printf("NGOTCHA LEN %lu NAME : %s \n", len, name );

	free(tstring);
	if (suffix_len == 0 ){
		return  name;
	}
	strcat( name, "." );
	strcat( name, suffix );

	return name;
}
char *mrc_get_time(void )
{
	time_t tm;
	tm = time( 0 );
	return ctime( &tm );
	
}
void mrc_print_line(void )
{
printf("#-------------------------------------------------------------------------------\n");
//      01234567890123456789012345678901234567890123456789012345678901234567890123456789
//      0        10        20        30        40        50        60        70 
}

void mrc_print_args( int argc, char *argv[] )
{
	int	i;
	printf("#COMMAND LINE: \n"); 
	for ( i=0; i < argc; i++){
		printf("%s ", argv[i] );
	}
	printf("\n");
}
void mrc_print_program_name( char *s )
{
	time_t tm;

	tm = time( 0 );
	
	mrc_print_line();
	printf("#- PROGRAM: %s \n", s);
//	printf("#- RUN DATE: %s", ctime( &tm ) );
	printf("#- RUN DATE: %s", mrc_get_time() );
	mrc_print_line();
}
