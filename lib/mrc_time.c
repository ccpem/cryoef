#include <stdio.h>
#include <time.h>
#ifdef __MINGW32__
#include <time.h>
#else
#include <sys/times.h>
#include <sys/time.h>
#include <time.h>
#endif

double mrc_time( void )
{
	double		val;
#ifdef __MINGW32__

	val = (double)clock();



#else
#ifdef OLD_TIMING
	struct tms 	tbuf;
	clock_t		t;

	t = times(&tbuf);

/*	val = ((double)tbuf.tms_utime)/((double)CLOCKS_PER_SEC); */

	val = 0.01*(double)tbuf.tms_utime;
#else		
	struct timeval tm;
	gettimeofday( &tm, NULL );
	val = tm.tv_sec + 0.000001*tm.tv_usec;
#endif
#endif

	return val;
}
double mrc_stime( void )
{
	double		val;
#ifdef __MINGW32__

	val = (double)clock();



#else
	struct tms 	tbuf;
	clock_t		t;

	t = times(&tbuf);

/*	val = ((double)tbuf.tms_utime)/((double)CLOCKS_PER_SEC); */

	val = 0.01*(double)tbuf.tms_stime;
#endif

	return val;
}


