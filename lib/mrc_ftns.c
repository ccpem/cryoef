#include "mrc_ftns.h"

#undef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#undef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))



//---------------------------------------------------------------
// mrc_save_map:: Always save as floating point 
//---------------------------------------------------------------
int 	mrc_save_map( char *filename, size_t data_size, 
			void *projection, int lda, int nx, int ny, int nz )
{

	double dmin, dmax, rms, dmean;
	int	i, j, k;
	int	mrc_id;
	int	ier;
	float	*fptr;
	double	tmp;
	size_t	len;
	double	sum;
	float	*fline_buf;

	mrc_id = mrc_new();
	
	ier = mrc_set_dimen( mrc_id, nx, ny, nz );
	if ( data_size == 4 || data_size == 8 ){
		ier = mrc_set_type_float(mrc_id);
	} else if ( data_size == 2 ){
		ier = mrc_set_type_short(mrc_id);
	} else if ( data_size == 1 ){
		ier = mrc_set_type_uchar(mrc_id);
	} else {
		mrc_error("Unknown data type in mrc_save_map");
	}

	ier = mrc_create_file( mrc_id, filename );

	ier = mrc_write_header( mrc_id);



	dmean = 0.0;
	rms = 0.0;
	dmin     =  10000000.0;
	dmax     = -10000000.0;
	if ( data_size == 4 ){
		float *fprojptr = (float *)projection;
		len  = nx*sizeof(float);
		for (k=0; k < nz; k++){
			for ( j=0; j < ny; j++){
				fptr = fprojptr  +  lda*(j + ny*k);
				for ( i=0; i < nx; i++){
					tmp = (double)fptr[i];
					dmin = MIN( dmin, tmp );
					dmax = MAX( dmax, tmp );
					dmean += tmp;
					rms += tmp*tmp;
				}
				ier = mrc_write_line( mrc_id, fptr, len );
			}
		}
	} else if ( data_size == 8 ){
		double  *dprojptr = (double *)projection;
		double	*dptr;


		len  = nx*sizeof(float);
		fline_buf = (float *)malloc(len);
		if ( fline_buf == NULL){
			printf("Failed to allocated fline_buf\n");
			exit(1);
		}
		for (k=0; k < nz; k++){
			for ( j=0; j < ny; j++){
				dptr = dprojptr  +  lda*(j + ny*k);
				for ( i=0; i < nx; i++){
					tmp = dptr[i];
					dmin = MIN( dmin, tmp );
					dmax = MAX( dmax, tmp );
					dmean += tmp;
					rms += tmp*tmp;
					fline_buf[i] = (float)tmp;
				}
				ier = mrc_write_line( mrc_id, fline_buf, len );
			}
		}

		free(fline_buf);

	} else if ( data_size == 1 ){
		unsigned char *ucptr;
		unsigned char *uc_ptr;

		ucptr = (unsigned char *)projection;
		len  = nx*sizeof(unsigned char);

		for (k=0; k < nz; k++){
			for ( j=0; j < ny; j++){
				uc_ptr = ucptr  +  lda*(j + ny*k);
				for ( i=0; i < nx; i++){
					tmp = uc_ptr[i];
					dmin = MIN( dmin, tmp );
					dmax = MAX( dmax, tmp );
					dmean += tmp;
					rms += tmp*tmp;
				}
				ier = mrc_write_line( mrc_id, uc_ptr, len );
			}
		}
	} else if ( data_size == 2 ){
		short *sptr;
		short *s_ptr;

		sptr = (short *)projection;
		len  = nx*sizeof(short);

		for (k=0; k < nz; k++){
			for ( j=0; j < ny; j++){
				s_ptr = sptr  +  lda*(j + ny*k);
				for ( i=0; i < nx; i++){
					tmp = s_ptr[i];
					dmin = MIN( dmin, tmp );
					dmax = MAX( dmax, tmp );
					dmean += tmp;
					rms += tmp*tmp;
				}
				ier = mrc_write_line( mrc_id, s_ptr, len );
			}
		}
	} else {
		printf("Unknown data size in save MRC file %lu \n", data_size);
		exit(1);
	}
 
	sum = dmean;
	dmean = dmean/((double)nx)/((double)ny)/((double)nz);
	rms = rms/((double)nx)/((double)ny)/((double)nz) - dmean*dmean;

	if ( rms > 0.0 ){
		rms = sqrt(rms);
	} else {
		rms = 0.0;
	}

	ier = mrc_write_max_min_mean_rms( mrc_id, dmax, dmin, dmean, rms );
	if ( ier != 0 ) {
		return -300;
	}

	ier = mrc_close(mrc_id);
	if ( ier != 0 ){
		return - 400;
	}



	return 0;

}
