#ifndef MRC_RANDOM
#define MRC_RANDOM

void	SeedRnd( unsigned long );

double	drnd(void);

void	rnd_gamma_set_alpha( long int seedval, double t );
double	rnd_gamma(void);

double	gasrnd( void );
double	q_gasrnd( double q );
double	students_t( double eta );
double	rnd_cos_sq( void );
double	rnd_rexp( double alpha );


#endif
