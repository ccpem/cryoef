/*****Finds only THE worst resolution*****/
double FindWorstRes (Array3D& dmap, int dim, int& phimax, int& thetamax)
{
    //center (DC) of R-space map after FT is at (dim/2,dim/2,dim/2)
    double rmax=0;
    int imax, jmax, kmax;
    double rad;
    int i, j, k;
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i)
                if(dmap(i,j,k)!=0)
                {
                    rad = sqrt(pow(i-dim/2,2.0)+pow(j-dim/2,2.0)+pow(k-dim/2,2.0));
                    if (rad>rmax)
                    {
                        rmax=rad;
                        imax=i-dim/2;
                        jmax=j-dim/2;
                        kmax=k-dim/2;
                    }
                }
    
    double phi, theta;
    Dir2Euler(imax/rmax, jmax/rmax, kmax/rmax, phi, theta);
    if (phi<0)
    {
        phi=phi+180;
        theta=180-theta;
    }
    thetamax=Quantize(theta, 1);
    phimax=Quantize(phi, 1);
    return rmax;
    
}

/*****Finds only THE best resolution*****/
double FindBestRes (Array3D& dmap, int dim, int& phimin, int& thetamin)
{
    //center (DC) of R-space map after FT is at (dim/2,dim/2,dim/2)
    double rmin=dim/2;
    int imin, jmin, kmin;
    double rad;
    int i, j, k;
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i)
                if(dmap(i,j,k)!=0)
                {
                    rad = sqrt(pow(i-dim/2, 2.0)+pow(j-dim/2, 2.0)+pow(k-dim/2, 2.0));
                    if (rad<rmin)
                    {
                        rmin=rad;
                        imin=i-dim/2;
                        jmin=j-dim/2;
                        kmin=k-dim/2;
                    }
                }
    
    double phi, theta;
    Dir2Euler(imin/rmin, jmin/rmin, kmin/rmin, phi, theta);
    if (phi<0)
    {
        phi=phi+180;
        theta=180-theta;
    }
    thetamin=Quantize(theta, 1);
    phimin=Quantize(phi, 1);
    return rmin;
}

void Interpolate (double (&resmap)[180][180])
{
    int thetaindex, phiindex, phiindex2;
    int index;
    int startpoint, endpoint;
    int length;
    double delta;
    bool checkstart, checkend;
    checkstart=0;
    checkend=0;
    bool flag;
    
    //TODO: REMOVE IF UNNECCESSARY
    /*std::ofstream mfile;
    mfile.open("ForPlot.txt");*/
    
    
    //THIS IS VERY CRUDE. TODO: COME UP WITH BETTER METHOD IF NECESSARY
    for (thetaindex=0; thetaindex<180; ++thetaindex)
    {
        for (phiindex=0; phiindex<180; ++phiindex)
        {
            if(resmap[phiindex][thetaindex]<=RoundingErr) resmap[phiindex][thetaindex]=0;
            if(resmap[phiindex][thetaindex]==0)
            {
                if (phiindex!=179)
                {
                    if(resmap[phiindex+1][thetaindex]!=0) resmap[phiindex][thetaindex]=resmap[phiindex+1][thetaindex];
                }
                else
                {
                    if (phiindex!=0)
                    {
                        if(resmap[phiindex-1][thetaindex]!=0) resmap[phiindex][thetaindex]=resmap[phiindex-1][thetaindex];
                    }
                    else
                    {
                        if (thetaindex!=179)
                        {
                            if(resmap[phiindex][thetaindex+1]!=0) resmap[phiindex][thetaindex]=resmap[phiindex][thetaindex+1];
                        }
                        else
                        {
                            if (thetaindex!=0) if(resmap[phiindex][thetaindex-1]!=0) resmap[phiindex][thetaindex]=resmap[phiindex][thetaindex-1];
                        }
                        
                    }
                }
            }
            /*if (resmap[phiindex][thetaindex]!=0)
                mfile << resmap[phiindex][thetaindex] << " " << phiindex << " " << thetaindex << std::endl;*/
        }
        /*mfile<<std::endl;*/
    }
    
    /*mfile.close();*/
}

//TODO: Improve (see idea from FindAngularDistribution in arrays3d.h)
/***** Function to find the angular distribution of resolution *****/
void ResolutionMap (Array3D& dmap, int dim, double (&resmap)[180][180], int da, double Apix)
{
    int i, j, k;
    double x, y, z;
    double rad;
    double theta, phi;
    int thetaindex, phiindex;
    
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i)
                if(dmap(i,j,k)!=0)
                {
                    rad=sqrt(pow(i-dim/2,2.0)+pow(j-dim/2,2.0)+pow(k-dim/2,2.0));
                    x=double(i-dim/2)/rad;
                    y=double(j-dim/2)/rad;
                    z=double(k-dim/2)/rad;
                    Dir2Euler(x, y, z, phi, theta);
                    if (phi<0)
                    {
                        phi=phi+180;
                        theta=180-theta;
                    }
                    thetaindex=Quantize(theta, da);
                    phiindex=Quantize(phi, da);
                    if (phiindex>=180)
                    {
                        phiindex=0;
                        thetaindex=180-thetaindex;
                    }
                    if (thetaindex>=180)
                    {
                        thetaindex=0;
                        phiindex=0;
                    }
                    resmap[phiindex][thetaindex]=rad*Apix;
                }
    Interpolate(resmap);
    
}

/*****Suggests tilt angles by calculating the range of better resolutions (around the weakest direction) accessible by tilting*****/
void SuggestTilt (int n, double (&resmap)[180][180], int phi1, int theta1, double tiltmax, int da, double res_worst, double Apix, std::ofstream& logfile)
{
    Matrix3x3 Rtilt;
    
    int coverage[180][180][2]={0};
    int thetaindex;
    int phiindex;
    double x1, y1, z1;
    Euler2Dir(phi1, theta1, x1, y1, z1);
    double theta2, phi2, x2, y2, z2;
    
    double theta, phi;
    for (theta=1; theta<tiltmax; ++theta)
        for(phi=0; phi<180; ++phi)
        {
            
            Euler2Matrix(phi+90, theta, -90-phi, Rtilt);
            Transform(Rtilt, x1, y1, z1, x2, y2, z2);
            Dir2Euler(x2, y2, z2, phi2, theta2);
            if (phi2<0)
            {
                phi2=phi2+180;
                theta2=180-theta2;
            }
            else if (phi2>=180)
            {
                phi2=0;
                theta2=180-theta2;
            }
            thetaindex=Quantize(theta2, da);
            phiindex=Quantize(phi2, da);
            if (phiindex>=0 && phiindex<180 && thetaindex>=0 && thetaindex<180)
            {
                coverage[phiindex][thetaindex][0]=theta;
                coverage[phiindex][thetaindex][1]=phi;
            }
        }
    
    int recom_tilt[3]={0};
    int recom_axis[3];
    double expect_res[3]={res_worst*Apix, res_worst*Apix, res_worst*Apix};
    
    
    for (thetaindex=0; thetaindex<180; ++thetaindex)
        for (phiindex=0; phiindex<180; ++phiindex)
        {
            if(resmap[phiindex][thetaindex]/cosd(coverage[phiindex][thetaindex][0])<expect_res[2] && coverage[phiindex][thetaindex][0]!=0 && resmap[phiindex][thetaindex]>RoundingErr)
            {
                if (resmap[phiindex][thetaindex]/cosd(coverage[phiindex][thetaindex][0])<expect_res[1])
                {
                    if (resmap[phiindex][thetaindex]/cosd(coverage[phiindex][thetaindex][0])<expect_res[0])
                    {
                        recom_tilt[0]=coverage[phiindex][thetaindex][0];
                        recom_axis[0]=coverage[phiindex][thetaindex][1];
                        expect_res[0]=resmap[phiindex][thetaindex]/cosd(coverage[phiindex][thetaindex][0]);
                    }
                    else
                    {
                        recom_tilt[1]=coverage[phiindex][thetaindex][0];
                        recom_axis[1]=coverage[phiindex][thetaindex][1];
                        expect_res[1]=resmap[phiindex][thetaindex]/cosd(coverage[phiindex][thetaindex][0]);
                    }
                }
                else
                {
                    recom_tilt[2]=coverage[phiindex][thetaindex][0];
                    recom_axis[2]=coverage[phiindex][thetaindex][1];
                    expect_res[2]=resmap[phiindex][thetaindex]/cosd(coverage[phiindex][thetaindex][0]);
                }
            }
        }
    
    bool found=false;
    for (int i=0; i<3-1; ++i)
    {
	for (int j=i+1; j<3; ++j)
	{
	    if (recom_tilt[j]<recom_tilt[i] + 2 && recom_tilt[j] > recom_tilt[i] - 2) recom_tilt[j]=0;
	}
    }
    logfile << std::endl << "----------------------------- Tilt Recommendations -----------------------------" << std::endl;
    for (int i=0; i<3; ++i)
        if (recom_tilt[i]!=0)
        {
	    if (i!=0) logfile << "or" << std::endl;
            logfile << "Recommended tilt angle: " << recom_tilt[i] << " deg" /* << " AROUND TILT AXIS AT: " << recom_axis[i] << " deg" << " WITH EXPECTED WORST RESOLUTION IMPROVEMENT UP TO " << expect_res[i] << "Å"*/ << std::endl;
	    logfile << "Recommended to collect ~" << std::setprecision(1) << 1+5*pow(sind(recom_tilt[i]),2.0) << " times more particles at this angle than at zero tilt" << std::endl;
	    // This is based on requiring res(tilt) = res(0), with res(0) ~ root(logN) and res(tilt) ~root(logNtilt) x cos(tilt_angle), expanding for small angles, assuming N~10^5
            found=true;
        }
    //Tilt dependence as 1/cos(angle) is only an approximation
    if(!found)
    {
        std::cerr << "No tilt recommendations available" << std::endl;
        logfile << "No tilt recommendations available" << std::endl;
    }
}
