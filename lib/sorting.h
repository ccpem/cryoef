/*****Counting sort algorithm for two parallel data structures******/
void CountSort(std::vector<int>& alpha, std::vector<int>& beta, int exp, int n)
{
    
    std::vector<int> output_alpha(n);
    std::vector<int> output_beta(n);
    int i, j;
    int count[10]={0};
    
    std::vector<int>::iterator it=alpha.begin();
    for(;it!=alpha.end();it++) count[(*it/exp)%10]++;
    
    for (i=1;i<10;++i) count[i]+=count[i-1];
    
    for (i=n-1; i>=0; --i)
    {
        output_alpha[count[(alpha[i]/exp)%10]-1]=alpha[i];
        output_beta[count[(alpha[i]/exp)%10]-1]=beta[i];
        count[(alpha[i]/exp)%10]--;
    }
    
    
    alpha=output_alpha;
    beta=output_beta;
    
    
}

/*****Radix sort invoking Count sort*****/
void RadixSort(std::vector<int>& alpha, std::vector<int>& beta, int n)
{
    for (int exp=1; exp<181; exp=exp*10) CountSort(alpha,beta,exp,n);
}