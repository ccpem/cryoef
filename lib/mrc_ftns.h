#ifndef MRC_FTNS
#define MRC_FTNS


#include "mrc.h"


int	mrc_save_map( char *filename, size_t data_size,
		void *map, int lda, int nx, int ny, int nz );


#endif
