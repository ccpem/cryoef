#ifndef MRC_INTERP_FTNS
#define MRC_INTERP_FTNS


#include "mrc.h"


float 	EvalSpline3f( float xx, float yy, 
		float *fdata, int lda, int nx, int ny );

int	GenerateCubicSplinesf( float *dbuf, int lda, int nx, int ny );


double EvalSpline3( double xx, double yy, 
		double *fdata, int lda, int nx, int ny );

int	GenerateCubicSplines( double *dbuf, int lda, int nx, int ny );



#endif
