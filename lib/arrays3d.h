/*****Custom class for easy indexing of 3D arrays*****/
class Array3D {
    size_t width, height, depth;
    std::vector<double> data;
public:
    Array3D(size_t x, size_t y, size_t z, double init = 0):
    depth(x), width(z), height(y), data(x*y*z, init)
    {}
    double& operator()(size_t x, size_t y, size_t z)
    {
        return data.at(x*width*height+y*width+z);
    }
    void print()
    {
        for (int i=0; i<depth; ++i)
        {
            for(int j=0; j<height; ++j)
                
            {
                for(int k=0; k<width; ++k)
                    
                {
                    std::cout<<std::setw(3)<<data[i*width*height+j*width+k]<<" ";
                    
                };
                std::cout<<std::endl;
            };
            std::cout<<std::endl<<std::endl;
        };
    }
};

/*****Normalization to sum of squares = 1*****/
void Normalize (Array3D& dmap, int dim)
{
    double sum=0;
    int i,j,k;
    for (i=0; i<dim; ++i)
        for (j=0; j<dim; ++j)
            for (k=0; k<dim; ++k) sum=sum+dmap(i,j,k)*dmap(i,j,k);
    
    sum=sqrt(sum);
    
    for (i=0; i<dim; ++i)
        for (j=0; j<dim; ++j)
            for (k=0; k<dim; ++k) dmap(i,j,k)=dmap(i,j,k)/sum;
}

/*****Normalization to sum=1 and then taking square roots*****/
void NormalizeSquared (Array3D& dmap, int dim) //(FOR K_SPACE)
{
    double sum=0;
    int i,j,k;
    for (i=0; i<dim; ++i)
        for (j=0; j<dim; ++j)
            for (k=0; k<dim; ++k) sum=sum+dmap(i,j,k);
    
    for (i=0; i<dim; ++i)
        for (j=0; j<dim; ++j)
            for (k=0; k<dim; ++k) dmap(i,j,k)=sqrt(dmap(i,j,k)/sum);
}

/*****Normalization to sum=1*****/
void NormalizePower (Array3D& dmap, int dim) //(FOR K_SPACE)
{
    double sum=0;
    int i,j,k;
    for (i=0; i<dim; ++i)
        for (j=0; j<dim; ++j)
            for (k=0; k<dim; ++k) sum=sum+dmap(i,j,k);
    
    for (i=0; i<dim; ++i)
        for (j=0; j<dim; ++j)
            for (k=0; k<dim; ++k) dmap(i,j,k)=dmap(i,j,k)/sum;
}

/*****Fourier transform*****/
void FourierTransform (Array3D& inmap, Array3D& outmap, int dim, int dimpad)
{
    int i, j, k;
    fftw_complex *ftarray;
    fftw_plan p;
    ftarray=(fftw_complex*) fftw_malloc(sizeof(fftw_complex)*pow(dim+dimpad,3.0));
    
    for (i=0; i<dim+dimpad; ++i)
        for (j=0; j<dim+dimpad; ++j)
            for (k=0; k<dim+dimpad; ++k)
            {
                ftarray[i+(dim+dimpad)*(j+k*(dim+dimpad))][0]=0;
                ftarray[i+(dim+dimpad)*(j+k*(dim+dimpad))][1]=0;
            }
    
    for (k=dimpad/2; k<dim+dimpad/2; ++k)
        for (j=dimpad/2; j<dim+dimpad/2; ++j)
            for (i=dimpad/2; i<dim+dimpad/2; ++i)
            {
                ftarray[i+(dim+dimpad)*(j+k*(dim+dimpad))][0]=inmap(i-dimpad/2,j-dimpad/2,k-dimpad/2)*pow(-1.0,i+j+k-3*dimpad/2);
                ftarray[i+(dim+dimpad)*(j+k*(dim+dimpad))][1]=0;
            }
    
    p=fftw_plan_dft_3d(dim+dimpad,dim+dimpad,dim+dimpad,ftarray,ftarray,FFTW_BACKWARD,FFTW_ESTIMATE);
    fftw_execute(p);
    
    for (k=dimpad/2; k<dim+dimpad/2; ++k)
        for (j=dimpad/2; j<dim+dimpad/2; ++j)
            for (i=dimpad/2; i<dim+dimpad/2; ++i) outmap(i-dimpad/2,j-dimpad/2,k-dimpad/2)=sqrt(pow(ftarray[i+(dim+dimpad)*(j+k*(dim+dimpad))][0],2.0)+pow(ftarray[i+(dim+dimpad)*(j+k*(dim+dimpad))][1],2.0));
    
    fftw_destroy_plan(p);
    fftw_free(ftarray);
}


/*****Thresholding: discards all values below some given threshold*****/
void Thresholding(Array3D& dmap, int dim, double thr)
{
    int i, j, k;
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i)
                if(dmap(i,j,k)<thr) dmap(i,j,k)=0;
}

/*****Extracts only the surface of a volume*****/
void GetSurface(Array3D& dmap, int dim)
{
    int i, j, k;
    Array3D smap(dim,dim,dim,0);
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i)
                if(dmap(i,j,k)!=0) smap(i,j,k)=1;
    
    for (k=1; k<dim; ++k)
        for (j=1; j<dim; ++j)
            for (i=1; i<dim; ++i)
                if(SameSigns(smap(i,j,k),smap(i-1,j,k),smap(i,j-1,k),smap(i,j,k-1),smap(i-1,j-1,k),smap(i-1,j,k-1),smap(i,j-1,k-1),smap(i-1,j-1,k-1))) dmap(i,j,k)=0;
                else dmap(i,j,k)=1;
    dmap(dim/2,dim/2,dim/2)=0;
}

/*****Calculates area as number of pixels on a surface*****/
int CalcArea (Array3D& surf, int dim)
{
    int area=0;
    int i, j, k;
    for (i=0; i<dim; ++i)
        for(j=0; j<dim; ++j)
            for(k=0; k<dim; ++k) if (surf(i,j,k)!=0) ++area;
    return area;
    
}

/*****Calculates volume as number of pixels inside a shape*****/
int CalcVolume(Array3D& dmap, int dim)
{
    int i, j, k;
    int calc=0;
    for (k=0; k<dim; ++k)
        for (j=0; j<dim; ++j)
            for (i=0; i<dim; ++i)
                if(dmap(i,j,k)!=0) ++calc;
    return calc;
}

/*****Finds radial derivative df/dr of a function f(x,y,z)*****/
void RadialDerivative (Array3D& dmap, Array3D& dfdr, int dim)
{
    int i, j, k;
    int ief, jef, kef;
    
    Array3D arr(dim,dim,dim,0);
    
    for (i=1; i<dim-1; ++i)
        for(j=1; j<dim-1; ++j)
            for(k=1; k<dim-1; ++k)
            {
                ief=i-dim/2;
                jef=j-dim/2;
                kef=k-dim/2;
                arr(i,j,k) = ((dmap(i+1,j,k)-dmap(i-1,j,k))*ief+(dmap(i,j+1,k)-dmap(i,j-1,k))*jef+(dmap(i,j,k+1)-dmap(i,j,k-1))*kef)/(2*sqrt(ief*ief+jef*jef+kef*kef));
            }
    
    //Gaussian smoothening
    for (i=2; i<dim-2; ++i)
        for (j=2; j<dim-2; ++j)
            for (k=2; k<dim-2; ++k)
                dfdr(i,j,k) = arr(i,j,k)+arr(i+1,j,k)/M_E+arr(i,j+1,k)/M_E+arr(i,j,k+1)/M_E+arr(i-1,j,k)/M_E+arr(i,j-1,k)/M_E+arr(i,j,k-1)/M_E+arr(i+1,j+1,k)/exp(2)+arr(i+1,j,k+1)/exp(2)+arr(i,j+1,k+1)/exp(2)+arr(i+1,j-1,k)/exp(2)+arr(i+1,j,k-1)/exp(2)+arr(i,j+1,k-1)/exp(2)+arr(i-1,j+1,k)/exp(2)+arr(i-1,j,k+1)/exp(2)+arr(i,j-1,k+1)/exp(2)+arr(i-1,j-1,k)/exp(2)+arr(i-1,j,k-1)/exp(2)+arr(i,j-1,k-1)/exp(2);
    
    for (i=2; i<dim-2; ++i)
        for (j=2; j<dim-2; ++j)
            for (k=2; k<dim-2; ++k)
                dfdr(i,j,k) = fabs(dfdr(i,j,k));
}

/*****Function to find significant holes in K-space*****/
void FindAngularDistribution (Array3D& dmap, int dim, std::ofstream& logfile)
{
    //in K-space map origin is at dim/2-0.5
    //std::ofstream fs;
    //fs.open("InfoDistrib.txt");
    double o=dim/2-0.5;
    int ro=dim/10; //TODO: CHOOSE ro=?
    int r;
    int i, j, k;
    double distrib [360][91];
    int count;
    double sum;
    double mean=0;
    int phi;
    int v;
    double theta[91];
    double x, y, z;
    double max=0;
    for (v=0; v<=90; ++v)
    {
        for (phi=0; phi<360; ++phi)
        {
            theta[v]=acos(2*double(v)/180.0/*-1*/)*180/M_PI; //to get uniform sampling on the sphere
            count=0;
            sum=0;
            Euler2Dir(phi,theta[v],x,y,z);
            for (r=ro; r<dim/2; r++)
            {
                i=Quantize(x*r+o,1);
                j=Quantize(y*r+o,1);
                k=Quantize(z*r+o,1);
                sum+=dmap(i,j,k);
                ++count;
            }
            distrib[phi][v]=sum/count;
            if (distrib[phi][v]>max) max=distrib[phi][v];
            mean+=distrib[phi][v];
            //fs << phi << " " << theta[v] << " " << distrib[phi][v] << std::endl;
        }
    }
    
    //TODO: FIX FILE PRINT ORDER
    /*
    for (phi=0; phi<360; ++phi)
        for (v=0; v<=90; ++v)
           fs << theta[v] << " " << phi << " " << distrib[phi][v] << std::endl;
    */
    mean=mean/(360*91);
    double stdev=0;
    for (v=0; v<=90; ++v)
        for (phi=0; phi<360; ++phi)
            stdev+=pow(distrib[phi][v]-mean,2.0);
    stdev=stdev/(91*360);
    stdev=sqrt(stdev);
    count=0;
    bool flag_major=0;
    bool flag_minor=0;
    
    for (v=0; v<90; ++v)
        for (phi=0; phi<360; ++phi)
        {
            //if (max-distrib[phi][v]>stdev)
            if (distrib[phi][v]<max/2)
            {
                //if (max-distrib[phi][v]>3*stdev)
                if (distrib[phi][v]<max/10)
                {
                    //std::cout << "!!! MAJOR GAP ALERT: PHI=" << phi << " THETA=" << theta[v] << std::endl;
                    ++count;
                    flag_major=1;
                }
                //else std::cout << "MILD GAP ALERT: PHI=" << phi << " THETA=" << theta[v] << std::endl;
                else flag_minor=1;
            }
        }
    
    if (flag_major)
    {
        std::cerr << "Major Fourier space gap alert" << std::endl;
        logfile << "Major Fourier space gap alert" << std::endl;
        logfile << "Fraction of Fourier space empty: " << std::setprecision(2) << double(count)/(91*360)*100 << "%" << std::endl;
    }
    else if (flag_minor)
    {
        std::cerr << "Minor Fourier space gap alert" << std::endl;
        logfile << "Minor Fourier space gap alert" << std::endl;
    }
    
    //std::cout << std::setprecision(4) << "MEAN: " << mean << std::endl;
    //std::cout << "STDEV: " << stdev << std::endl;
    
    
    //fs.close();
}
