#ifndef MRC_TYPES
#define MRC_TYPES

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#ifdef __MINGW32__
#include <winsock.h>
#define ELOOP  WSAELOOP
#endif


#ifdef __MINGW32__
#ifndef ssize_t
typedef long ssize_t;
#endif
#endif


#include "mrc_dimen.h"

#define MRC_HEADER_LENGTH	1024

#define MRC_NUM_LABEL_MAX	10
#define	MRC_LABEL_LENGTH	80


typedef enum {
	MRC_UCHAR,
	MRC_SHORT,
	MRC_USHORT,
	MRC_FLOAT,
	MRC_DOUBLE
} MRC_Type;

/*
 * MRC IMAGE header, organized as 56 LONG words (4 bytes)
 * followed by space for 10 80 byte text labels.
 *
 * NOTE: there is some confusion in my mind about the format of
 *       mode = 0 images. I believe these are actually unsigned
 *       and so form 0 to 255 rather than signed.
 */


typedef struct {
	int	nx;	/* Number of columns (fastest changing in map )		*/
	int	ny;	/* Number of rows					*/
	int	nz;	/* Number of sections (slowest changing in map) 	*/

	int	mode; 	/* data type:
				0 image : unsigned 8-bit bytes range 0-255
				1       : 16-bit halfwords
				2       : floats
				3       : complex 16-bit integers
				4       : complex 32-bit reals
			*/
				

	int	nxstart;	/* number of first column in map (Default = 0 )  */
	int	nystart; 	/* number of frist row in map			*/
	int	nzstart;	/* number of first section in map		*/
	
	int	mx;		/* number of intervals along X			*/
	int	my;		/* number of intervals along Y			*/
	int	mz;		/* number of intervals along Z			*/

	float	cella[3];	/* cell dimension in angstrons			*/
	float	cellb[3];	/* cell angles in degrees			*/

	int	mapc;		/* axis correspoind to cols (1,2,3 for xy, z )	*/
	int	mapr;		
	int	maps;

	float	dmin;		/* minimum density value 			*/
	float	dmax;		/* maximum density value 			*/
	float	dmean;		/* mean density value 				*/

	int	ispg;		/* Space group number 0 or 1 */
	int	nsymbt;		/* Number of bytes used for symmetry data */

	int	extra[25];
	float	origin[3];	/* Origin in X,Y,Z used for transforms */

	char	map[4];		/* Charcter string 'MAP ' to identify file type */
	char	machst[4]; 	/* Machine stamp */


	float	rms;		/* rms deviation of map from mean density 	*/
	int	nlabel;		/* Number of lablels being used */
	char	label[MRC_NUM_LABEL_MAX][MRC_LABEL_LENGTH];

} MRC_Header;



typedef struct {
	int		fp;
	char		*filename;
	int		byte_swap;
	off_t		file_size;
	off_t		data_pos;
	size_t		data_size;
	MRC_Type	type;
	MRC_Header	header;
	int		nx, ny, nz;
	char		*symmetry_info;
} MRC_File;



#endif
