/*****Accuracy of calculations*****/
const double RoundingErr=1e-5; 

/*****3x3 Transformation matrices class*****/
const int dm=3;
class Matrix3x3 {
    
    std::vector<double> data;
public:
    Matrix3x3(double init=0):
    data(dm*dm, init)
    {}
    double& operator()(size_t x, size_t y)
    {
        return data.at(x*dm+y);
    }
    void print()
    {
        for (int i=0; i<dm; ++i)
        {
            for (int j=0; j<dm; ++j)
                std::cout<<std::setw(5)<<data.at(i*dm+j)<<" ";
            std::cout<<std::endl;
        }
    }
    
    void operator=(Matrix3x3 M)
    {
        for (int i=0; i<dm; ++i)
            for (int j=0; j<dm; ++j)
                data.at(i*dm+j)=M(i,j);
    }
    
    Matrix3x3 operator*(Matrix3x3 M)
    {
        Matrix3x3 R;
        for (int i=0; i<dm; ++i)
            for (int j=0; j<dm; ++j)
                for (int k=0; k<dm; ++k)
                    R(i,j)+=data.at(i*dm+k)*M(k,j);
        
        for (int i=0; i<dm; ++i)
            for (int j=0; j<dm; ++j)
            {
                if(fabs(R(i,j))<RoundingErr) R(i,j)=0;
                else if (fabs(1-fabs(R(i,j)))<RoundingErr)
                {if(R(i,j)>0) R(i,j)=1; else R(i,j)=-1;}
            }
        
        return R;
    }
    
    Matrix3x3 operator*(double x)
    {
        Matrix3x3 R;
        for (int i=0; i<dm; ++i)
            for (int j=0; j<dm; ++j)
                R(i,j)=data.at(i*dm+j)*x;
        
        for (int i=0; i<dm; ++i)
            for (int j=0; j<dm; ++j)
                if(fabs(R(i,j))<RoundingErr) R(i,j)=0;
        
        return R;
    }
    
    Matrix3x3 operator+(Matrix3x3 M)
    {
        Matrix3x3 R;
        for (int i=0; i<dm; ++i)
            for (int j=0; j<dm; ++j)
                R(i,j)=data.at(i*dm+j)+M(i,j);
        
        for (int i=0; i<dm; ++i)
            for (int j=0; j<dm; ++j)
                if(fabs(R(i,j))<RoundingErr) R(i,j)=0;
        
        return R;
    }
    
    bool operator==(Matrix3x3 M)
    {
        for (int i=0; i<dm; ++i)
            for (int j=0; j<dm; ++j)
                if (fabs(data.at(i*dm+j)-M(i,j))>RoundingErr) return 0;
        return 1;
    }
    
    Matrix3x3 topower(int n)
    {
        Matrix3x3 I;
        I(0,0)=1;
        I(1,1)=1;
        I(2,2)=1;
        
        if(n==0) return I;
        
        Matrix3x3 R;
        R=*this;
        Matrix3x3 Zero;
        
        for (int m=1; m<=n; ++m)
            I=I*R;
        return I;
    }
    
};

/*****calculates sin of input angle in degrees*****/
double sind (double angle)
{
    double sine=sin(angle*M_PI/180);
    if (fabs(sine)<RoundingErr) return 0;
    return sine;
}

/*****calculates cos of input angle in degrees*****/
double cosd (double angle)
{
    double cosine=cos(angle*M_PI/180);
    if (fabs(cosine)<RoundingErr) return 0;
    return cosine;
}

/*****Binning of numerical data*****/
double Quantize (double x, int q)
{
    int x_quantized=x/q;
    if (x/double(q)-x_quantized>=0.5) ++x_quantized;
    return x_quantized*q;
}

/*****Euler angles to Cartesian vector components*****/
void Euler2Dir (double phi, double theta, double& x, double& y, double& z)
{
    x=cosd(phi)*sind(theta);
    y=sind(phi)*sind(theta);
    z=cosd(theta);
}

/*****Cartesian vector components to Euler angles*****/
void Dir2Euler (double x, double y, double z, double& phi, double& theta)
{
    theta=acos(z)*180/M_PI;
    phi=atan2(y,x)*180/M_PI;
}

/*****Euler angles to rotation matrix*****/
void Euler2Matrix (double phi, double theta, double psi, Matrix3x3& R)
{
    double sinphi=sind(phi);
    double cosphi=cosd(phi);
    double sintheta=sind(theta);
    double costheta=cosd(theta);
    double sinpsi=sind(psi);
    double cospsi=cosd(phi);
    
    R(0,0)=cospsi*costheta*cosphi-sinpsi*sinphi;
    R(0,1)=cospsi*costheta*sinphi+sinpsi*cosphi;
    R(0,2)=0-cospsi*sintheta;
    R(1,0)=0-sinpsi*costheta*cosphi-cospsi*sinphi;
    R(1,1)=cospsi*cosphi-sinpsi*costheta*sinphi;
    R(1,2)=sinpsi*sintheta;
    R(2,0)=sintheta*cosphi;
    R(2,1)=sintheta*sinphi;
    R(2,2)=costheta;
}

/*****Performs coordinate transformation on a given point with a given transform. matrix*****/
void Transform(Matrix3x3& R, double x1, double y1, double z1, double& x2, double& y2, double& z2)
{
    double r1[3]={x1,y1,z1};
    double r2[3]={0,0,0};
    for (int i=0; i<3; ++i)
        for(int j=0; j<3; ++j)
            r2[i]+=R(i,j)*r1[j];
    x2=r2[0];
    y2=r2[1];
    z2=r2[2];
}

/*****Compares the signs of eight points*****/
bool SameSigns (int p1, int p2, int p3, int p4, int p5, int p6, int p7, int p8)
{
    if (p2!=p1) return 0;
    if (p3!=p1) return 0;
    if (p4!=p1) return 0;
    if (p5!=p1) return 0;
    if (p6!=p1) return 0;
    if (p7!=p1) return 0;
    if (p8!=p1) return 0;
    return 1;
}

