#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>

#include "mrc_types.h"

#include "mrc_io.h"
#include "mrc_error.h"
#include "mrc_utils.h"

#ifdef __MINGW32__
#define fsync(__fd) ((FlushFileBuffers ((HANDLE) _get_osfhandle (__fd))) ? 0 : -1)
#endif






static	MRC_File 	files[MRC_MAX_FILES];
static	int		file_map[MRC_MAX_FILES];

static	int		init_file_list = 1;

static	char		label_buf[MRC_LABEL_LENGTH+1];

static int FindFileEntry(void)
{
	int 	i;

	if ( init_file_list ){
		for ( i=0; i < MRC_MAX_FILES; i++){
			file_map[i] 		= -1;
			files[i].fp 		= -1;
			files[i].filename 	= NULL;
			files[i].symmetry_info 	= NULL;
		}
		init_file_list = 0;
	}

	for ( i=0; i < MRC_MAX_FILES; i++){
		if ( file_map[i] < 0){
			return i;
		}
	}
	return -1;
}

int mrc_init( int mrc_id )
{
	MRC_Header	*header;
	char		*cptr;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("mrc_init: Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	header->nx = 0;
	header->ny = 0;
	header->nz = 0;

	header->mode = -1;

	header->mx = 0;
	header->my = 0;
	header->mz = 0;

	header->nxstart = 0;
	header->nystart = 0;
	header->nzstart = 0;


	
	header->cella[0] = 1.0;
	header->cella[1] = 1.0;
	header->cella[2] = 1.0;

	header->cellb[0] = 90.0;
	header->cellb[1] = 90.0;
	header->cellb[2] = 90.0;

	header->mapc =  1;
	header->mapr =  2;
	header->maps =  3;

	header->ispg   =  0;
	header->nsymbt =  0;
	
	header->dmin  =  0.0;
	header->dmax  =  0.0;
	header->dmean =  0.0;
	header->rms   =  0.0;

	
	header->origin[0] =  0.0;
	header->origin[1] =  0.0;
	header->origin[2] =  0.0;

	strncpy(header->map, "MAP ", 4 );
/*
 * NOTE: all files written by this library will
 * appear to come from a little-endian machine.
 * The alphas "DA" machine stamp has been used.
 */
	strncpy(header->machst, "DA", 2 );

	header->nlabel = 0;
	cptr = &header->label[0][0];
	memset( cptr, 0, MRC_NUM_LABEL_MAX*MRC_LABEL_LENGTH );


	return 0;
}


int mrc_new( void )
{
	int		mrc_id;
	int		ier;

	mrc_id = FindFileEntry();
	if ( mrc_id < 0 ) {
		mrc_error("Can not find an mrcfile entry");
		return -1;
	}
	file_map[mrc_id] = 1;
	ier = mrc_init( mrc_id);
	if ( ier < 0 ) {
		mrc_error("Trouble initializing mrc file");
		return -2;
	}

	return mrc_id;
}

/*
 * mrc_test_mrc_map:
 * Test if the "MAP" parameter is stored in the header
 */
int mrc_test_mrc_map( const char *file_name )
{
	int 		fo;
	ssize_t		rlen;
	mode_t		mode;
	int		flags;
	MRC_Header	*header;
	unsigned 	char buf[MRC_HEADER_LENGTH];


	flags = O_RDONLY;  
/**	flags |= O_CREAT; ***/
#ifdef __MINGW32__
	mode = S_IRUSR | S_IWUSR;
	flags |=  O_BINARY;  
#else
	mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
#endif


	fo = open( file_name, flags, mode );
	if ( fo == -1 ){
		mrc_warning("Unable to open file %s", file_name);
		return -1;
	}

	rlen = read( fo, buf, MRC_HEADER_LENGTH );
	if ( rlen == -1 ){
		switch( errno ){
			case EAGAIN:
				printf("Non-blocking I/O has been selected and no data was present \n");
				break;
			case EBADF:
				printf("In valid file descriptor\n");
				break;
			case EINTR:
				printf("Interrupted read\n");
				break;
			case EIO:
				printf("I/O error\n");
				break;
			case EISDIR:
				printf("fd refers to a director\n");
				break;
			default:
				printf("Unknown errno %d \n", errno );
				break;
	
		}
		printf("Errno = %d \n", errno );
		mrc_error("Reading MRC HEADER structure rlen=%d", rlen);
#ifdef DEBUG
	} else {
		printf(" READ HEADER LENGTH %d \n", rlen );
#endif
	}
	
	close(fo);

/*
 * Now test if "MAP" is positioned correcty...
 */

	header = (MRC_Header *)buf;

	if ( strncmp(header->map, "MAP ", 4 ) != 0 ){
		mrc_warning("MAP not present .... "); 
		return -1;
	}


	return 1;

}

int mrc_open( const char *file_name, int flags )
{
	int 		fo;
	int		mrc_id;
	MRC_Header	*header;
	ssize_t		rlen;
	mode_t		mode;
	int		ier;
	struct stat	stat_buf;
	off_t 		test_size;
	int		i;


	

	/*
	 * First get the stats about the file
	 */
	ier = stat( file_name, &stat_buf );
	if ( ier < 0 ){
		switch( errno ){
			case EBADF:
				mrc_warning("Unable to stat file %s: filedes is bad", file_name);
				break;
			case ENOENT:
				mrc_warning("Unable to stat file %s: A component of the path does not exist, or the path is an empty string", file_name);
				break;
			case ENOTDIR:
				mrc_warning("Unable to stat file %s: A component of the path is not a directory", file_name);
				break;
			case ELOOP:
				mrc_warning("Unable to stat file %s: Too many symbolic links encountered", file_name);
				break;
			case EFAULT:
				mrc_warning("Unable to stat file %s: Bad address denied", file_name);
				break;
			case EACCES:
				mrc_warning("Unable to stat file %s: Permission denied", file_name);
				break;
			case ENOMEM:
				mrc_warning("Unable to stat file %s: Out of memory", file_name);
				break;
			case ENAMETOOLONG:
				mrc_warning("Unable to stat file %s: File name is too long", file_name);
				break;
			default:
				mrc_warning("Unable to stat file %s -- don't know why!", file_name);
				break;
		}
		return -1;
	} 


	if ( flags == 0 ){
		flags = O_RDONLY;  
	}
/**	flags |= O_CREAT; ***/
#ifdef __MINGW32__
	mode = S_IRUSR | S_IWUSR;
	flags |=  O_BINARY;  
#else
	mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
#endif
	fo = open( file_name, flags, mode );
	if ( fo == -1 ){
		mrc_error("Unable to open file %s", file_name);
		return -1;
	}


	mrc_id = FindFileEntry();
	if ( mrc_id < 0 ) {
		return -1;
	}


	files[mrc_id].file_size =  stat_buf.st_size;


	header = &files[mrc_id].header;
	rlen = read( fo, header, MRC_HEADER_LENGTH );
	if ( rlen == -1 ){
		switch( errno ){
			case EAGAIN:
				printf("Non-blocking I/O has been selected and no data was present \n");
				break;
			case EBADF:
				printf("In valid file descriptor\n");
				break;
			case EINTR:
				printf("Interrupted read\n");
				break;
			case EIO:
				printf("I/O error\n");
				break;
			case EISDIR:
				printf("fd refers to a director\n");
				break;
			default:
				printf("Unknown errno %d \n", errno );
				break;
	
		}
		printf("Errno = %d \n", errno );
		mrc_error("Reading MRC HEADER structure rlen=%d", rlen);
#ifdef DEBUG
	} else {
		printf(" READ HEADER LENGTH %d \n", rlen );
#endif
	}




	files[mrc_id].filename = strdup(file_name);
	files[mrc_id].fp = fo;
	file_map[mrc_id] = 1;


	
	files[mrc_id].byte_swap = 0;
	for ( i=0; i < 2; i++ ){
		test_size = 0;
//		printf("TRY ENDIAN %d : %d %d %d \n", i, header->nx, header->ny, header->nz );
		if ( header->nx > 0 && header->ny > 0 && header->nz > 0 ){
			test_size = ((off_t)header->nx) * ((off_t)header->ny) * ((off_t)header->nz);

			switch( header->mode ){
				case	0:
					files[mrc_id].type = MRC_UCHAR;
					files[mrc_id].data_size = sizeof(unsigned char);
					break;
				case	1:
					files[mrc_id].type = MRC_SHORT;
					files[mrc_id].data_size = sizeof(short);
					break;
				case	2:
					files[mrc_id].type = MRC_FLOAT;
					files[mrc_id].data_size = sizeof(float);
					break;
				default:
					printf(">>>>>>>>>>>>>>>>>>>> WHY header mode %d  \n", header->mode );
					files[mrc_id].data_size = 0;
			}


			test_size *= (off_t)files[mrc_id].data_size;


			test_size += (off_t)MRC_HEADER_LENGTH;


			test_size += (off_t)header->nsymbt;	

		}


//		printf("Test_size %zu file[mrc_id].file_size %zu \n", test_size, files[mrc_id].file_size );
	
		if ( test_size >= files[mrc_id].file_size ) {
			if ( test_size == files[mrc_id].file_size ){
				break;
			} else  if ( test_size - files[mrc_id].file_size < 10000 ){
				break;
			}
		}

		if ( test_size < files[mrc_id].file_size ) {
			if ( files[mrc_id].file_size - test_size < 10000 ){
				break;
			}
		}
		mrc_swap_header( header );
		files[mrc_id].byte_swap = 1;
	}


#ifdef FISH	
	if ( test_size != files[mrc_id].file_size ){
		files[mrc_id].byte_swap = 1;
		mrc_warning("Can't find the correct endian-ness or file size is wrong: test_size %ld file_size %ld ", test_size, files[mrc_id].file_size);
		
//		ier = mrc_close( mrc_id );
//		return -1;
	}
#endif



	if ( header->nsymbt > 0 ){
		printf("Reading an extra %d symmetry bytes \n", header->nsymbt );
		files[mrc_id].symmetry_info = (char *)malloc( header->nsymbt );
		rlen = read( fo, files[mrc_id].symmetry_info, (size_t)header->nsymbt );
	} else {
		files[mrc_id].symmetry_info = NULL;
	}



	files[mrc_id].data_pos  =  MRC_HEADER_LENGTH + header->nsymbt;


	return mrc_id;	
	
}

int mrc_close( int mrc_id )
{
	int fo;


	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	fo = files[mrc_id].fp;

	if ( files[mrc_id].symmetry_info != NULL ){
		free( files[mrc_id].symmetry_info );
		files[mrc_id].symmetry_info = NULL;
	}

	files[mrc_id].fp 	= -1;
	file_map[mrc_id] 	= -1;
		

	fsync(fo);

	close(fo);

	return 0;
}

int mrc_set_type_double( int mrc_id )
{
	return mrc_set_type( mrc_id, MRC_DOUBLE);
}
int mrc_set_type_float( int mrc_id )
{
	return mrc_set_type( mrc_id, MRC_FLOAT);
}
int mrc_set_type_ushort( int mrc_id )
{
	return mrc_set_type( mrc_id, MRC_USHORT);
}
int mrc_set_type_short( int mrc_id )
{
	return mrc_set_type( mrc_id, MRC_SHORT);
}
int mrc_set_type_uchar( int mrc_id )
{
	return mrc_set_type( mrc_id, MRC_UCHAR);
}

int mrc_set_type( int mrc_id, int type )
{
	MRC_Header	*header;
	int		mode;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("mrc_set_dimen: Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	mode = -1;
	switch(type){
		case MRC_UCHAR:
			mode = 0;
			break;
		case MRC_USHORT:
			mode = 1;
			break;
		case MRC_SHORT:
			mode = 1;
			break;
		case MRC_FLOAT:
			mode = 2;
			break;
		case MRC_DOUBLE:
			mode = 6;
			break;
	}

//	printf("SETTING MODE to %d \n", mode );

	header->mode = mode;

	return 0;
}
int mrc_set_dimen( int mrc_id, int nx, int ny, int nz )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("mrc_set_dimen: Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;
	header->nx = nx;
	header->ny = ny;
	header->nz = nz;
/*
 * Also setting mx, my, and mz to be equal to the corresponding
 * nx, ny, and nz values.
 */
	header->mx = nx;
	header->my = ny;
	header->mz = nz;

	return 0;
}

int mrc_get_dimen( int mrc_id, int *nx, int *ny, int *nz )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;
	*nx = header->nx;
	*ny = header->ny;
	*nz = header->nz;


	return 0;
}

int mrc_get_mdimen( int mrc_id, int *mx, int *my, int *mz )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	*mx = header->mx;
	*my = header->my;
	*mz = header->mz;


	return 0;
}
int mrc_set_mdimen( int mrc_id, int mx, int my, int mz )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	header->mx = mx;
	header->my = my;
	header->mz = mz;


	return 0;
}

int mrc_get_nstart( int mrc_id, int *nxstart, int *nystart, int *nzstart )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	*nxstart = header->nxstart;
	*nystart = header->nystart;
	*nzstart = header->nzstart;

	return 0;
}

int mrc_get_max_min_mean_rms( int mrc_id, double *dmax, double *dmin, double *dmean, double *rms)
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	*dmax  = (double)header->dmax;
	*dmin  = (double)header->dmin;
	*dmean = (double)header->dmean;
	*rms   = (double)header->rms;

	return 0;
}

size_t mrc_get_data_size( int mrc_id )
{
	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return 0;
	}


	return files[mrc_id].data_size;
}

int mrc_read_line( int mrc_id, void *buf )
{
	MRC_Header	*header;
	int 		fo;
	ssize_t		rlen;
	size_t		len;
	size_t		itst;
	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return 0;
	}

	fo = files[mrc_id].fp;
	header = &files[mrc_id].header;
	len = header->nx * files[mrc_id].data_size;


	rlen = read( fo, buf, len);
	if ( rlen == -1 ){
		mrc_error("Error reading line");
		return -1;
	} else {
		itst = (size_t)rlen;
		if ( itst != len ){
			mrc_error("Error reading line: expect %u got %u error %d", len, itst, errno);
		}
	}


	if ( files[mrc_id].byte_swap ){
		switch( files[mrc_id].type ){
			case MRC_FLOAT:
				break;
			case MRC_USHORT:
				break;
			case MRC_SHORT:
				break;
			case MRC_DOUBLE:
				break;
			case MRC_UCHAR:
				break;
			default:
				mrc_error("Unknown data type %d", files[mrc_id].type );
				len = 0;
			
		}
	}

	return (int)len;
}

int mrc_rewind(  int mrc_id )
{
	int 		fo;
	off_t		new_pos;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	fo = files[mrc_id].fp;

	new_pos = lseek( fo, files[mrc_id].data_pos, SEEK_SET );
	if ( new_pos != files[mrc_id].data_pos ){
		mrc_error("Rewinding the file %d ", mrc_id);
		return -2;
	}

	return 0;
}


int mrc_dup( int mrc_id )
{
	int 		new_mrc_id;
	MRC_Header	*header, *new_header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}

	new_mrc_id = FindFileEntry();
	if ( new_mrc_id < 0 ) {
		return -2;
	}
	header 		= &files[mrc_id].header;
	new_header 	= &files[new_mrc_id].header;

	memcpy( new_header, header, sizeof(MRC_Header));

	files[new_mrc_id].fp = -1;
	file_map[new_mrc_id] = 1;

	return new_mrc_id;
}

int mrc_create_file( int mrc_id,  const char *file_name )
{
	int 		fo;
	int		flags;
	mode_t		mode;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	if ( files[mrc_id].filename != NULL ){
		free(files[mrc_id].filename);
		files[mrc_id].filename = NULL;
	}


/***TEST GREG 	flags = O_WRONLY | O_CREAT | O_TRUNC; ***/
	flags = O_RDWR | O_CREAT | O_TRUNC;
#ifdef __MINGW32__
        flags |= O_BINARY;
	mode = S_IRUSR | S_IWUSR;
#else
	mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
#endif
	fo = open( file_name, flags, mode );
	if ( fo == -1 ){
		files[mrc_id].fp = -1;
		return -1;
	}


	files[mrc_id].fp = fo;
	files[mrc_id].filename = strdup(file_name);
	return 0;

}

int mrc_write_header( int mrc_id )
{
	int 		fo;
	MRC_Header	*header;
	ssize_t		len;
	off_t		new_pos;
#ifdef WORDS_BIGENDIAN
	unsigned char	cbuf[MRC_HEADER_LENGTH];
#endif

	if ( file_map[mrc_id] != 1 ){
/***		mrc_error("Something wrong with list of files"); ***/
		return -1;
	}
	fo 	= files[mrc_id].fp;
	header 	= &files[mrc_id].header;

	new_pos = lseek( fo, 0, SEEK_SET );

#ifdef WORDS_BIGENDIAN
/*
 * As the header has been faked to be little-endian
 * on a big endian machine the header bytes have to 
 * be swapped.
 */
	memcpy( cbuf, header, MRC_HEADER_LENGTH );
	mrc_swap_header( (MRC_Header *)&cbuf[0] );

#endif

	len = write( fo, header, sizeof(MRC_Header));


	if ( len != sizeof(MRC_Header ) ){
/**		mrc_error("Trouble writing header"); **/
		return -2;
	}

	if ( header->nsymbt > 0 ){
		printf("Writing extra %d symmetry bytes \n", header->nsymbt );
		files[mrc_id].symmetry_info = (char *)malloc( header->nsymbt );
		len = write( fo, files[mrc_id].symmetry_info, header->nsymbt );
		if ( len != header->nsymbt ){
			mrc_error("Trouble writing header symmetry bits");
		}
	}

	return 0;

}
int mrc_write_line( int mrc_id, const void *buf, size_t len )
{
	int 		fo;
	ssize_t		rlen;
	if ( file_map[mrc_id] != 1 ){
		mrc_error("mrc_write_line: Something wrong with list of files");
		return -1;
	}

	fo = files[mrc_id].fp;

	rlen = write( fo, buf, len );
	if ( rlen != len ){
		mrc_error("mrc_write_line: writing line %d %d", len, rlen);
		return -1;
		
	}

	return 0;
}

int mrc_write_max_min_mean_rms( int mrc_id, double tmax, double tmin, double tmean, double rms )
{
	int 		fo;
	int		ier;
	off_t		new_pos;
	off_t		curr_pos;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	fo = files[mrc_id].fp;


	curr_pos = lseek( fo, 0, SEEK_CUR );


	ier = mrc_set_max_min_mean_rms(  mrc_id, tmax,  tmin, tmean, rms);

	new_pos = lseek( fo, 0, SEEK_SET );
	ier = mrc_write_header( mrc_id );

	new_pos = lseek( fo, curr_pos, SEEK_SET );

	return 0;

}

int mrc_set_max_min_mean_rms( int mrc_id, double dmax, double dmin, double dmean, double rms)
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	header->dmax  = (float)dmax;
	header->dmin  = (float)dmin;
	header->dmean = (float)dmean;
	header->rms   = (float)rms;

	return 0;
}

int mrc_print_header( int mrc_id )
{
	MRC_Header	*header;
	int		i;
	char		buf[128];

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	printf("#-----------------------------------------------------------------\n");
	printf("#---MRC-FILE-INFORMATION------------------------------------------\n");
	printf("#-----------------------------------------------------------------\n");
	printf("# NAME: %s \n", files[mrc_id].filename );
	printf("#\n");
	printf("# Number of columns, rows, sections ..... %d %d %d\n",
		 header->nx, header->ny, header->nz );
	printf("# Map mode .............................. %d\n", header->mode );
	printf("# Bytes per entry........................ %zu\n", files[mrc_id].data_size );
	printf("# Start points on columns ...............\n");
	printf("# Grid sampling on x, y, z ..............\n");
	printf("# Cell axes ............................. %f %f %f\n",
			header->cella[0], header->cella[1], header->cella[2]);
	printf("# Cell angles ........................... %f %f %f\n",
			header->cellb[0], header->cellb[1], header->cellb[2]);
	printf("# Fast, medium, slow axes \n");
	printf("# Origin on x,y: %f %f %f\n",
		header->origin[0], header->origin[1], header->origin[2]);

	printf("# Minimum density:   %f\n", header->dmin);
	printf("# Maximum density:   %f\n", header->dmax);
	printf("# Mean  density:     %f\n", header->dmean);
	printf("# Rms:               %f\n", header->rms);
	printf("#\n");
	printf("# Symmetry %d  Extra bytes %d\n", header->ispg, header->nsymbt );
	for ( i=0; i < header->nsymbt; i += 80 ){
		strncpy( buf, &files[mrc_id].symmetry_info[i], 80 );
		printf("%s\n", buf );
	}
	printf("# Number of labels:%d\n", header->nlabel);
	if ( header->nlabel > 0 ){
		for ( i=0; i < header->nlabel; i++){
			strncpy( buf, header->label[i], MRC_LABEL_LENGTH );
			buf[80] = '\0';
			printf("#%2d: %s\n",i+1, buf );
		}
	}
	printf("#-----------------------------------------------------------------\n");

	return 0;
}
/*
 * mrc_write_block writes an awkward block of data to file
 */
int mrc_write_block( int mrc_id, const unsigned char *buf, size_t lda, int m, int n, int ipos, int jpos,
		size_t data_size )
{
	int 		fo;
	off_t		offset;
	off_t		new_pos;
	int		nx;
	size_t		count;
	int		i;
	ssize_t		slen;
	size_t		ip;
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("mrc_write_line: Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;


	fo = files[mrc_id].fp;


	nx = header->nx;

	/*
	 * Watch out the off_t's here were needed to persuade the compiler to do the right
	 * thing.
 	 */


	offset = data_size*((off_t)(nx))*((off_t)(jpos-n+1));
        offset += data_size*ipos;
	offset += 1024;		/* header */


	new_pos = lseek( fo, offset, SEEK_SET );
	if ( (off_t)new_pos != (off_t)offset ){
		mrc_error("mrc_write_block: Unable to position file: %lu %lu ", new_pos, offset);
		exit(1);
		
	}

	count = m*data_size;
	for ( i=0; i < n; i++ ){
		ip = data_size*lda*( n - 1 - i );
		if ( i > 0 ){
			offset = data_size*(nx-m);
			new_pos = lseek( fo, offset, SEEK_CUR );
		}
		slen = write( fo, &buf[ip], count );
	}

	return 0;
}

int mrc_get_origin( int mrc_id, double *origin )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}

	header = &files[mrc_id].header;

	/*
	 * Note origin in the header structure is a float.
	 */


	origin[0] = header->origin[0];
	origin[1] = header->origin[1];
	origin[2] = header->origin[2];

	return 0;
}
int mrc_set_origin( int mrc_id, double *origin )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}

	header = &files[mrc_id].header;


	/*
	 * Note header in origin is a float
	 */

	header->origin[0] = origin[0];
	header->origin[1] = origin[1];
	header->origin[2] = origin[2];


	return 0;
}
int mrc_add_label( int mrc_id, const char *label )
{
	MRC_Header	*header;
	int		nlabel;
	char		*cptr;
	size_t		len;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}

	header = &files[mrc_id].header;

	nlabel = header->nlabel;
	if ( nlabel >= MRC_NUM_LABEL_MAX ) {
		return -1;
	}

	len = strlen( label );
	if ( len > 80 ){
		len = 80;
	}

	strncpy( header->label[nlabel], label, len);
	cptr = header->label[nlabel];
	while ( len < 80 ){
		cptr[len++] = 0x20;
	}


	nlabel++;
	header->nlabel = nlabel;;


	return nlabel;
}
/*
 * mrc_get_resolution: Look for string Res. <number> in the
 *                     comment strings. The <number> is assumed
 *                     to be the resolution in microns.
 */
int mrc_get_resolution( int mrc_id, double *res )
{
	MRC_Header	*header;
	int		nlabel;
	int		i;
	char		*cptr;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}

	/*
	 * Rubbish value
	 */

	*res = -1.0;

	header = &files[mrc_id].header;
	nlabel = header->nlabel;
	if ( nlabel <= 0 ) {
		return -1;
	}

	for ( i=0; i < header->nlabel; i++){
		if ( (cptr = strstr(header->label[i], "Res." )) != NULL){
			printf(" found the resolution string \n");
			sscanf( cptr, "Res. %lf ", res );
			return 0;
		}
	}
	printf(" Unable to find the resolution \n");


	return -1;
}
int mrc_get_fp( int mrc_id )
{
	int		fp;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	fp = files[mrc_id].fp;

	return fp;
}

int mrc_read_block( int mrc_id, void *buf, size_t len )
{
	MRC_Header	*header;
	int 		fo;
	ssize_t		rlen;
	size_t		nleft = len;
	unsigned char	*ptr;
	int		ic;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return 0;
	}

	fo = files[mrc_id].fp;
	header = &files[mrc_id].header;

	ic = 0;
	ptr = buf;
	while ( nleft > 0 && ic < 1000 ){
		ic++;
		if ( (rlen = read( fo, ptr, nleft)) < 0 ){
			mrc_warning("mrc_read_block: Unable to read data");
			return -1;
		} else if ( rlen == 0 ){
			mrc_warning("mrc_read_block: Hit the end of the file");
			break;
		}
		nleft -= rlen;
		ptr   += rlen;
	}

	return (int)(len-nleft);
}

int mrc_write_data( int mrc_id, void *buf, size_t len )
{
	int 		fo;
	ssize_t		rlen;
	size_t		count = (size_t)len;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return 0;
	}

	fo = files[mrc_id].fp;
	if ( (rlen = write( fo, buf, count)) < 0 ){
		mrc_warning("mrc_read_block: Unable to write data");
		return -1;
	}
	fsync(fo);

	return (int)(rlen);
}

int mrc_save_data( char *filename, int type, void *x, int lda, int nx, int ny )
{

	int	file_id;
	int	ier;
	int	nz = 1;
	int	i, j;
	float 	*fptr;
	double	dmin, dmax;
	double	rms, dmean;
	double	tmp;
	size_t	len;


	file_id = mrc_new();
	if ( file_id < 0 ) {
		mrc_warning("Unable to get a mrc file header ");
		return -1;
	}

	ier  = mrc_create_file( file_id, filename );
	if ( ier < 0 ) {
		mrc_warning("Unable to create a mrc file");
		return -2;
	}


	ier = mrc_set_dimen( file_id, nx, ny, nz );
	if ( ier < 0 ) {
		mrc_warning("Unable to set dimensions of mrc file");
		return -3;
	}

	if (type == MRC_FLOAT  || type == MRC_DOUBLE ){
		ier = mrc_set_type_float(file_id); 
		if ( ier < 0 ) {
			mrc_warning("Unable to set type to float ");
			return -4;
		}
	} else if (  type == MRC_UCHAR ){
		ier = mrc_set_type_uchar(file_id); 
		if ( ier < 0 ) {
			mrc_warning("Unable to set type to uchar ");
			return -4;
		}
	} else if (  type == MRC_SHORT ){
		ier = mrc_set_type_short(file_id); 
		if ( ier < 0 ) {
			mrc_warning("Unable to set type to short ");
			return -4;
		}
	} else {
		mrc_error("Not set up for this type yet: %d \n", type );
	}


	ier = mrc_write_header(file_id);
	if ( ier < 0 ) {
		mrc_warning("Unable to write header ");
		return -5;
	}


	dmin =  1.0e30; 
	dmax = -1.0e30; 
	dmean = 0.0;
	rms   = 0.0;
	if (type == MRC_FLOAT ){
		len = sizeof(float)*nx;
		fptr = (float *)x;
		for ( j=0; j < ny; j++){
			for ( i=0; i < nx; i++){
				tmp = (double)fptr[i];
				dmean += tmp;
				rms   += tmp*tmp;
				if ( tmp < dmin ){ dmin = tmp; }
				if ( tmp > dmax ){ dmax = tmp; }
			}
			ier = mrc_write_line( file_id, fptr, len );
			fptr += lda;
		}
	} else if (type == MRC_DOUBLE ){
		float 	*fbuf;
		double	*dptr;

		len = sizeof(float)*nx;
		fbuf = (float *)malloc( len );

		dptr = (double *)x;
		for ( j=0; j < ny; j++){
			for ( i=0; i < nx; i++){
				tmp = (double)dptr[i];
				fbuf[i] = (float)tmp;
				dmean += tmp;
				rms   += tmp*tmp;
				if ( tmp < dmin ){ dmin = tmp; }
				if ( tmp > dmax ){ dmax = tmp; }
			}
			ier = mrc_write_line( file_id, fbuf, len );
			dptr += lda;
		}
	}  else if (type == MRC_UCHAR ){
		unsigned char *ucptr;
		len = sizeof(unsigned char)*nx;

		ucptr = (unsigned char *)x;
		for ( j=0; j < ny; j++){
			for ( i=0; i < nx; i++){
				tmp = (double)ucptr[i];
				dmean += tmp;
				rms   += tmp*tmp;
				if ( tmp < dmin ){ dmin = tmp; }
				if ( tmp > dmax ){ dmax = tmp; }
			}
			ier = mrc_write_line( file_id, ucptr, len );
			ucptr += lda;
		}

	}  else if (type == MRC_SHORT ){
		short *sptr;
		len = sizeof(short)*nx;

		sptr = (short *)x;
		for ( j=0; j < ny; j++){
			for ( i=0; i < nx; i++){
				tmp = (double)sptr[i];
				dmean += tmp;
				rms   += tmp*tmp;
				if ( tmp < dmin ){ dmin = tmp; }
				if ( tmp > dmax ){ dmax = tmp; }
			}
			ier = mrc_write_line( file_id, sptr, len );
			sptr += lda;
		}
	}

	dmean = dmean/((double)(nx*ny));
	rms = rms/((double)(nx*ny)) - dmean*dmean;

	if ( rms > 0.0 ){
		rms = sqrt(rms);
	} else {
		rms = 0.0;
	}


	ier = mrc_set_max_min_mean_rms( file_id, dmax, dmin, dmean, rms );


	ier = mrc_write_header( file_id);
	if ( ier < 0 ){
		mrc_warning("Unable to rewrite header ");
		return -7;
	}

	ier = mrc_close( file_id );


	return 0;
}

void *mrc_get_header( int mrc_id )
{
	int 		fo;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return NULL;
	}
	fo = files[mrc_id].fp;

	return (void *)&files[mrc_id].header;

}
int mrc_get_cell_dimen( int mrc_id, double *cella )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}


	header = &files[mrc_id].header;

	cella[0] = header->cella[0];
	cella[1] = header->cella[1];
	cella[2] = header->cella[2];


	return 0;
}
int mrc_set_cell_dimen( int mrc_id, double *cella )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}


	header = &files[mrc_id].header;

	header->cella[0]  = cella[0];
	header->cella[1]  = cella[1];
	header->cella[2]  = cella[2];


	return 0;
}

int mrc_get_label_number( int mrc_id )
{
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return -1;
	}
	header = &files[mrc_id].header;

	return header->nlabel;
}

char *mrc_get_label( int mrc_id, int n )
{
	int 		nlabel;
	MRC_Header	*header;

	if ( file_map[mrc_id] != 1 ){
		mrc_error("Something wrong with list of files");
		return NULL;
	}
	header = &files[mrc_id].header;
	nlabel =  header->nlabel;

	if ( n <= 0 || n > nlabel ){
		return NULL;
	}

	strncpy( label_buf, header->label[n-1], MRC_LABEL_LENGTH );
	/*
 	 * Make sure it is null terminated
 	 */
	label_buf[MRC_LABEL_LENGTH] = '\0';

	return label_buf;
	
}


int mrc_read_page_to_double(int mrc_id, size_t data_size, 
			double *dbuf, size_t lda, int nx, int ny, 
			unsigned char *line_buf )
{
	int		ier;
	int		i, j;
	double		*dptr;

	if ( data_size == 4 ){
		float	*fptr;
		for ( j=0; j < ny; j++){
			dptr = dbuf + j*lda;
			ier = mrc_read_line( mrc_id, line_buf );
			fptr = (float *)line_buf;
			if ( ier < 0 ){ return ier; }
			for ( i=0; i < nx; i++){
				dptr[i] = (double)fptr[i];
			}
		}
	} else if ( data_size == 1 ){
		unsigned char *cptr;
		for ( j=0; j < ny; j++){
			dptr = dbuf + j*lda;
			ier = mrc_read_line( mrc_id, line_buf );
			cptr = (unsigned char *)line_buf;
			if ( ier < 0 ){ return ier; }
			for ( i=0; i < nx; i++){
				dptr[i] = (double)cptr[i];
			}
		}
	} else if ( data_size == 2 ){
		short	*sptr;
		for ( j=0; j < ny; j++){
			dptr = dbuf + j*lda;
			ier = mrc_read_line( mrc_id, line_buf );
			sptr = (short *)line_buf;
			if ( ier < 0 ){ return ier; }
			for ( i=0; i < nx; i++){
				dptr[i] = (double)sptr[i];
			}
		}
	} else {
		return -1;
	}

	return 0;
}
int mrc_read_page_to_float(int mrc_id, size_t data_size, 
			float *fbuf, size_t lda, int nx, int ny, 
			unsigned char *line_buf )
{
	int		ier;
	int		i, j;
	float		*fptr;

	if ( data_size == 4 ){
		for ( j=0; j < ny; j++){
			fptr = fbuf + j*lda;
			ier = mrc_read_line( mrc_id, fptr );
		}
	} else if ( data_size == 1 ){
		unsigned char *cptr;
		for ( j=0; j < ny; j++){
			fptr = fbuf + j*lda;
			ier = mrc_read_line( mrc_id, line_buf );
			cptr = (unsigned char *)line_buf;
			if ( ier < 0 ){ return ier; }
			for ( i=0; i < nx; i++){
				fptr[i] = (float)cptr[i];
			}
		}
	} else if ( data_size == 2 ){
		short	*sptr;
		for ( j=0; j < ny; j++){
			fptr = fbuf + j*lda;
			ier = mrc_read_line( mrc_id, line_buf );
			sptr = (short *)line_buf;
			if ( ier < 0 ){ return ier; }
			for ( i=0; i < nx; i++){
				fptr[i] = (float)sptr[i];
			}
		}
	} else {
		return -1;
	}

	return 0;
}
