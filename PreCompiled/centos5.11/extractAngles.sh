#!/bin/bash

if [ "$1"  == "" ]; then
 echo ""
 echo "extractAngles.sh"
 echo ""
 echo "is a simple script to extract particles orientation angles from"
 echo "RELION starfiles for analysis with cryoEF."
 echo "See Naydenova & Russo 2017 for more information."
 echo ""
 echo "Usage: extractAngles.sh inputstarfile.star"
 echo "where the inputstarfile.star is the datafile (_data.star)" 
 echo "of any 3D reconstruction from RELION (3D Class or 3D autorefine)"
 echo ""
 echo "Example:"
 echo "extractAngles.sh relion_run2_data.star > angles.dat"
 echo "cryoEF -f angles.dat"
 echo ""
 exit
fi

starfile=$1;
thetacol=`grep _rlnAngleTilt ${starfile} | awk '{print $2}' | sed 's/#//'`
phicol=`grep _rlnAngleRot ${starfile} | awk '{print $2}' | sed 's/#//'`
cat ${starfile} | awk -v thetacolvar=${thetacol} -v phicolvar=${phicol} '{if (NF>2) print $phicolvar, $thetacolvar}'
