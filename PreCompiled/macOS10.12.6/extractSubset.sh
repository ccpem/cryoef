#!/bin/bash

if [ "$1" == "" ]; then
 echo ""
 echo "extractSubset.sh"
 echo ""
 echo "is a simple script to extract a small subset of"
 echo "particle orientation angles from a list,"
 echo "which allows quicker analysis of the orientation distribution."
 echo ""
 echo "Usage: extractSubset.sh number inputdatafile.dat"
 echo "where number is the size of the subset"
 echo "and inputdatafile.dat is the list of orientation angles"
 echo "possibly created with the script extractAngles.sh"
 echo ""
 echo "Example:"
 echo "extractAngles.sh relion_run2_data.star > angles.dat"
 echo "extractSubset.sh 1000 angles.dat > angles_subset.dat"
 echo "cryoEF -f angles_subset.dat"
 echo ""
 echo "See Naydenova & Russo 2017 for more information."
 echo ""
 exit
fi

shuf -n $1 $2
