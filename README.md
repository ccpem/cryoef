# cryoEF

cryoEF is a command-line tool for analysing the orientation distribution of single-particle EM data according to the algorithm described in Naydenova and Russo, 2017