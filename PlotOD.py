###### PlotOD.py #####
# Version 0.1
#Mollweide projection plot of orientation distribution
#Copyright (c) 2017 Christopher J. Russo, Katerina Naydenova, MRC Laboratory of Molecular Biology
#Run with the command: python3 PlotOD.py NameOfYourDataFile

#Import all libraries to be used. If you do not have python 3 and/or all the libraries, consider installing the anaconda python3 distribution ( https://anaconda.org/anaconda/python ), which includes all these libraries.
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import font_manager
import matplotlib
import sys
import os

#Set up font - Helvetica, size 7 (this only works on the LMB network)
fprop = font_manager.FontProperties(fname = '/lmb/home/knayde/anaconda3/lib/python3.6/site-packages/matplotlib/mpl-data/fonts/ttf/Helvetica.ttf', size = 7)

#Standard figure widths for publications
OneColW = 3.54 # 9 cm in inches
TwoColW = 7.09 # 18 cm in inches

if len(sys.argv) == 1:
	sys.exit("Error! Run with the command supplying the name of your input file: python3 PlotOD.py NameOfYourDataFile")

#Read the two-column orientation angle (phi, theta) file used by cryoEF. The name (possibly with path) of the file is passed from the command line.
EulerAngles = pd.np.array(pd.read_csv(sys.argv[1], header = None, delim_whitespace = True))

#Convert degrees to radians and obey angular range conventions
x = EulerAngles[:,0]/180*np.pi #x is the phi angle (longitude)
y = EulerAngles[:,1]/180*np.pi #y is the theta angle (lattitude)
y = -1*y + np.pi/2 #The convention in RELION is [0, 180] for theta, whereas for the projection function it is [90, -90], so this conversion is required.

#Create the figure
fig = plt.figure(figsize = (TwoColW, TwoColW/2), dpi = 300) #can choose OneColW or TwoColW here

#Create the Mollweide projection plot on a white background
ax = fig.add_subplot(111, projection="mollweide", axisbg = 'white')

#Plot your points on the projection
ax.plot(x,y, ',', alpha = 0.5, color = '#64B5F6') #alpha - transparency (from 0 to 1), color - specify hex code

#Draw the horizontal and the vertical grid lines. Can add more grid lines if required.
major_ticks_x = [-np.pi, -np.pi/2, 0, np.pi/2, np.pi]
major_ticks_y = [-np.pi/2, -np.pi/4, 0, np.pi/4, np.pi/2]
ax.set_xticks(major_ticks_x)
ax.set_yticks(major_ticks_y)

#Remove the labels of the grid lines
ax.set_xticklabels(['','','','',''])
ax.set_yticklabels(['','','','',''])

#Uncomment these two lines if you want the grid lines labeled. If you changed the number and positions of the grid lines above, you have to make the same changes here.
#ax.set_xticklabels(['-180$^\circ$','-90$^\circ$','0$^\circ$','90$^\circ$','180$^\circ$'], fontproperties = fprop, color = 'grey')
#ax.set_yticklabels(['-90$^\circ$','-45$^\circ$','0$^\circ$','45$^\circ$','90$^\circ$'], fontproperties = fprop, color = 'grey')

#Set the color and the thickness of the grid lines
ax.grid(which = 'both', linestyle = '--', linewidth = 1, color = '#555F61') #linestytle options include '--' for dashed, ':' for dotted, '-' for continuous, '.-' for dot-dash; linewidth is the line thickness; color - specify hex code (this is grey)

#Set the color and the thickness of the outlines
for child in ax.get_children():
    if isinstance(child, matplotlib.spines.Spine):
        child.set_color('#555F61') #specify color by hex code (this is grey)

#Save the figure as OD.pdf in the current directory
plt.tight_layout()
figname = os.path.splitext(sys.argv[1])[0] + ".pdf"
fig.savefig(figname, bbox_inches = 'tight') #can change the file name here to something different
print("Figure saved as:")
print(figname)
